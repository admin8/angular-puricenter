import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { NotifierService } from 'angular-notifier';
import { Debt } from 'src/app/interfaces/debt';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { DebtService } from 'src/app/service/debt.service';

@Component({
  selector: 'app-list-debt',
  templateUrl: './list-debt.component.html',
  styleUrls: ['./list-debt.component.css']
})
export class ListDebtComponent implements OnInit {
  private notifier: NotifierService;

  dataSource:MatTableDataSource<Debt>;
  displayedColumns: string[] = ['Options', 'Client', 'Quantity', 'Price', 'Route'];
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  isLoadingResults = false;
  resultsLength: any = 0;
  pageIndex: any = 0;
  search: Debt = { Client: '', Quantity: null, Price: null, Route: '' };

  constructor(
    private debtService: DebtService,
    private auth: AuthService,
    notifier: NotifierService
  ) {
    this.notifier = notifier;
    this.loadDebt();
   }
  ngOnInit() {
  }
  loadDebt($order = {active: 'Client', direction: 'asc'}, $paginator = {pageIndex: 0, pageSize: 25}) {
    this.isLoadingResults = true;
    if (!Object.entries($order).length) {
      $order = { active: 'Client', direction: 'asc' };
    }
    if (!Object.entries($paginator).length) {
      $paginator = {pageIndex: 0, pageSize: 25};
    } else {
      this.pageIndex = $paginator.pageIndex;
    }
    this.debtService.get( $order, $paginator, this.search)
        .subscribe((res: any) => {
          this.isLoadingResults     = false;
          this.dataSource           =  res.results;
          this.dataSource.sort      = this.sort;
          this.dataSource.paginator = this.paginator;
          this.resultsLength        = res.SumRecords;

          this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
        }, error => {
          console.log(error);
          if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
            this.auth.logout();
          }
        });
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
