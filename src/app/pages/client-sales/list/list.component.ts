import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { NotifierService } from 'angular-notifier';
import { MatPaginator, MatSort, MatTableDataSource, MatCheckboxModule } from '@angular/material';
import { SalesClient } from 'src/app/interfaces/sales-client';
import { SalesClientService } from 'src/app/service/sales-client.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  private notifier: NotifierService;

  dataSource: MatTableDataSource<SalesClient>;
  displayedColumns: string[] = [ 'Options', 'Client', 'ByBill', 'Route'];
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false} ) sort: MatSort;
  isLoadingResult =  false;
  resultsLength: any = 0;
  pageIndex: any = 0;
  search: SalesClient = { Client: '', ByBill: null, Route: null };

  constructor(
      private auth: AuthService,
      notifier: NotifierService,
      private salesService: SalesClientService)
  {
    this.notifier = notifier;
    this.loadSales();
  }

  ngOnInit() {
  }
  loadSales($order = {active: 'Client', direction: 'asc'}, $paginator = {pageIndex: 0, pageSize: 25}) {
      this.isLoadingResult = true;
      if (!Object.entries($order).length) {
        $order = { active: 'Client', direction: 'asc' };
      }
      if (!Object.entries($paginator).length) {
        $paginator = {pageIndex: 0, pageSize: 25};
      } else {
        this.pageIndex = $paginator.pageIndex;
      }
      this.salesService.get( $order, $paginator, this.search)
          .subscribe((res: any) => {
            this.isLoadingResult      = false;
            this.dataSource           = res.results;
            this.dataSource.sort      = this.sort;
            this.dataSource.paginator = this.paginator;
            this.resultsLength        = res.SumRecords;
            this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
          }, error => {
            console.log(error);
            if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
              this.auth.logout();
            }
          });
  }

}
