import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { NotifierService } from 'angular-notifier';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { SalesDetail } from 'src/app/interfaces/sales-detail';
import { SalesClientService } from 'src/app/service/sales-client.service';
import { BillDialogComponent } from '../bill-dialog/bill-dialog.component';
import { DialogComponent } from 'src/app/partials/dialog/dialog.component';
@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
  private notifier: NotifierService;

  dataSource: MatTableDataSource<SalesDetail>;
  displayedColumns: string[] = ['Options', 'Date', 'Note', 'Quantity', 'Price',
                                'Total', 'BillNumber', 'BillDate', 'UserBill'];
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  isLoadingResults = false;
  resultsLength: any = 0;
  pageIndex: any = 0;
  nameClient: string;
  checked = [];
  search: SalesDetail = { Date: '', Note: '', Quantity: null, Price: null, Total: '', User: '', Route: '',
                         BillNumber: '', RecordsBill: '', BillDate: '', UserBill: '', DateInin: '', DateEnd: '', ViewAllNotes: 0 };
  constructor(
    private salesServices: SalesClientService,
    private auth: AuthService,
    notifier: NotifierService,
    public route: ActivatedRoute,
    public dialog: MatDialog
  ) {
    this.notifier = notifier;
    this.loadSalesDetail();
   }
  ngOnInit() {
  }
  loadSalesDetail($order = {active: 'Date', direction: 'asc'}, $paginator = {pageIndex: 0, pageSize: 25}) {
    this.isLoadingResults = true;
    if (!Object.entries($order).length) {
      $order = { active: 'Date', direction: 'asc' };
    }
    if (!Object.entries($paginator).length) {
      $paginator = {pageIndex: 0, pageSize: 25};
    } else {
      this.pageIndex = $paginator.pageIndex;
    }
    const $IdClient = this.route.snapshot.paramMap.get('IdClient');
    if (this.search.Date) {
      this.search.Date = moment(this.search.Date).format('YYYY-MM-DD');
    }
    if (this.search.RecordsBill) {
      this.search.RecordsBill = moment(this.search.RecordsBill).format('YYYY-MM-DD');
    }
    if (this.search.DateInin) {
      this.search.DateInin = moment(this.search.DateInin).format('YYYY-MM-DD');
    }
    if (this.search.DateEnd) {
      this.search.DateEnd = moment(this.search.DateEnd).format('YYYY-MM-DD');
    }
    this.salesServices.getDetail($IdClient, $order, $paginator, this.search)
        .subscribe((res: any) => {
          this.isLoadingResults     = false;
          this.dataSource           =  res.results;
          this.dataSource.sort      = this.sort;
          this.dataSource.paginator = this.paginator;
          this.resultsLength        = res.SumRecords;
          this.nameClient           = res.results[0].Client;
          this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
        }, error => {
          console.log(error);
          if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
            this.auth.logout();
          }
        });
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  showDialogBill() {
    if ( this.checked.length ) {
      const dialogRef = this.dialog.open(BillDialogComponent, {
        minWidth: '350px',
        data: {}
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result.success === 'confirm') {
          const date = moment(result.date).format('YYYY-MM-DD');
          const dat = {Details: this.checked, BillNumber : result.billNum, BillDate: date };
          this.salesServices.saveBill(dat)
          .subscribe((res: any) => {
            this.checked = [];
            this.loadSalesDetail();
            this.notifier.notify( res.result, res.alert );
          }, (e) => console.log(e));
        }
      });
    } else {
      this.notifier.notify( 'info', 'Favor de seleccionar las notas para facturar' );
    }
  }
  resetoreBill() {
    if ( this.checked && this.checked.length ) {
      const dialogRef = this.dialog.open(DialogComponent, {
        minWidth: '350px',
        data: {title: `Eliminar datos de factura`, content: `¿Desea eliminar los datos de facturacion de las notas seleccionadas ?`}
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result === 'confirm') {
          const dat = { Details: this.checked };
          this.salesServices.delete( dat )
              .subscribe((res: any) => {
                this.checked = [];
                this.loadSalesDetail();
                this.notifier.notify( res.result, res.alert );
              }, (e) => console.log(e));
        }
      });
    } else {
      this.notifier.notify( 'info', 'Favor de seleccionar las notas que se les va a elmininar los datos de facturacón' );
    }
  }
  checkedIn(IdDetail?: number, check?: boolean) {
    if (check) {
      this.checked.push(IdDetail);
    } else {
      for ( let i = 0; i < this.checked.length; i++) {
        if ( this.checked[i] === IdDetail) {
          this.checked.splice(i, 1);
        }
      }
    }
  }
}
