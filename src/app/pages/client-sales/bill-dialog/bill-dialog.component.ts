import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NotifierService } from 'angular-notifier';

@Component({
  selector: 'app-bill-dialog',
  templateUrl: './bill-dialog.component.html',
  styleUrls: ['./bill-dialog.component.css']
})
export class BillDialogComponent implements OnInit {
  form: FormGroup;
  private notifier: NotifierService;
  constructor(
    public dialog: MatDialog,
    private dialogRef: MatDialogRef<BillDialogComponent>,
    private formBuilder: FormBuilder,
    notifier: NotifierService,
    @Inject(MAT_DIALOG_DATA) {} ) {
      this.notifier = notifier;
      this.form =  this.formBuilder.group({
        BillDat: ['', Validators.required],
        BillNumber: ['', [ Validators.required ]]
      });
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  saveBill() {
    if (this.form.valid) {
      this.dialogRef.close({ success: 'confirm', date: this.form.value.BillDat, billNum: this.form.value.BillNumber });
    } else {
      this.notifier.notify( 'error', 'Favor de llenar todos los campos' );
    }
  }
  ngOnInit() {
    this.dialogRef.updatePosition({top: '50px'});
  }
}
