import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { NotifierService } from 'angular-notifier';
import { Debt } from 'src/app/interfaces/debt';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { TrustingService } from 'src/app/service/trusting.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  private notifier: NotifierService;

  dataSource:MatTableDataSource<Debt>;
  displayedColumns: string[] = ['Options', 'Client', 'Quantity', 'Total', 'Route'];
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  isLoadingResults = false;
  resultsLength: any = 0;
  pageIndex: any = 0;
  search: any = { Client: '', Quantity: '', Total: '', Route: '' };

  constructor(
    private trusting: TrustingService,
    private auth: AuthService,
    notifier: NotifierService
  ) {
    this.notifier = notifier;
    this.loadDebt();
   }
  ngOnInit() {
  }
  loadDebt($order = {active: 'Client', direction: 'asc'}, $paginator = {pageIndex: 0, pageSize: 25}) {
    this.isLoadingResults = true;
    if (!Object.entries($order).length) {
      $order = { active: 'Client', direction: 'asc' };
    }
    if (!Object.entries($paginator).length) {
      $paginator = {pageIndex: 0, pageSize: 25};
    } else {
      this.pageIndex = $paginator.pageIndex;
    }
    this.trusting.getAllTrust( $order, $paginator, this.search)
        .subscribe((res: any) => {
          this.isLoadingResults     = false;
          this.dataSource           = res.results;
          this.dataSource.sort      = this.sort;
          this.dataSource.paginator = this.paginator;
          this.resultsLength        = res.SumRecords;
          this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
        }, error => {
          console.log(error);
          if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
            this.auth.logout();
          }
        });
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
