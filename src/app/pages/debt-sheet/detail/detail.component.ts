import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';
import { NotifierService } from 'angular-notifier';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { DebtDetail } from 'src/app/interfaces/debt-detail';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { TrustingService } from 'src/app/service/trusting.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
  private notifier: NotifierService;

  dataSource:MatTableDataSource<DebtDetail>;
  displayedColumns: string[] = ['Options', 'Date', 'Note', 'Quantity', 'Price', 'Total', 'Name', 'DateRecords', 'Route'];
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  isLoadingResults = false;
  resultsLength: any = 0;
  pageIndex: any = 0;
  nameClient: string;
  search: DebtDetail = { Date: '', Note: '', Quantity: null, Price: null, Total: '', Name: '', DateRecords: '', Route: '' };

  constructor(
    private trusting: TrustingService,
    private auth: AuthService,
    notifier: NotifierService,
    public route: ActivatedRoute
  ) {
    this.notifier = notifier;
    this.loadDebtDetail();
   }
  ngOnInit() {
  }
  loadDebtDetail($order = {active: 'Date', direction: 'asc'}, $paginator = {pageIndex: 0, pageSize: 25}) {
    this.isLoadingResults = true;
    if (!Object.entries($order).length) {
      $order = { active: 'Date', direction: 'asc' };
    }
    if (!Object.entries($paginator).length) {
      $paginator = {pageIndex: 0, pageSize: 25};
    } else {
      this.pageIndex = $paginator.pageIndex;
    }
    const $Id_Client = this.route.snapshot.paramMap.get('IdClient');
    if (this.search.Date) {
      this.search.Date = moment(this.search.Date).format('YYYY-MM-DD');
    }
    if (this.search.DateRecords) {
      this.search.DateRecords = moment(this.search.DateRecords).format('YYYY-MM-DD');
    }
    this.trusting.getDetail($Id_Client, $order, $paginator, this.search)
        .subscribe((res: any) => {
          this.isLoadingResults     = false;
          this.dataSource           =  res.results;
          this.dataSource.sort      = this.sort;
          this.dataSource.paginator = this.paginator;
          this.resultsLength        = res.SumRecords;
          this.nameClient = res.results[0].Client;
          this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
        }, error => {
          console.log(error);
          if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
            this.auth.logout();
          }
        });
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
