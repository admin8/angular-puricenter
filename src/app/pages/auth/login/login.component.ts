import { Component, OnInit, ɵConsole } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/service/auth.service';
import { NotifierService } from 'angular-notifier';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  private notifier: NotifierService;
  form = new FormGroup( {
    user: new FormControl('', Validators.required),
    password:  new FormControl('', Validators.required)
  } );
  constructor(private auth: AuthService,
              notifier: NotifierService) {
                this.notifier = notifier;
  }
  ngOnInit() {
  }
  login() {
    if (this.form.valid) {
      this.auth.login(this.form.value)
          .subscribe((res: any) => {
            this.auth.saveToken(res);
          }, (e) => {
            this.form.get('password').reset();
            this.notifier.notify( 'error', 'HO HO! Algo salio mal. Valida tu usuario y contraseña.' );
          });
    }
  }
}
