import { Component, Input } from '@angular/core';
import { AuthService } from './service/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public login: boolean;
  role = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')).type : null;
  constructor(private auth: AuthService) {
    this.isLoggedIn();
  }
  isLoggedIn() {
    this.login =  this.auth.isLoggedIn();
    return  this.login;
  }
  logout() {
    this.login = false;
    return this.auth.logout();
  }
}