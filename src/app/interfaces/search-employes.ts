export interface SearchEmployes {
    Name?: string;
    Position?: string;
    Salary?: number;
    Route?: string;
    Active?: number;
}
