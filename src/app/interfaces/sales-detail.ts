export interface SalesDetail {
    Id_Detail?: number;
    Date?: string;
    Note?: string;
    Quantity?: number;
    Price?: number;
    Total?: string;
    User?: string;
    Route?: string;
    Name?: string;
    BillNumber?: string;
    BillDate?: string;
    RecordsBill?: string;
    UserBill?: string;
    DateInin?: string;
    DateEnd?: string;
    ViewAllNotes?: number;
}