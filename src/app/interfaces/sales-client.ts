export interface SalesClient {
    Id_Client?: number;
    Client?: string;
    ByBill?: number;
    Route?: string;
}