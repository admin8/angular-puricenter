export interface Cuts {
    Id_Cut?: number;
    Date: Date;
    Day: string;
    Fk_IdPeriod?: number;
    DateRecords?: string;
    Delete?: string;
    DateUpdate?: string;
}