export interface Periodos {
    Id_Period?: number;
    DateInin: Date;
    DateEnd: string;
    Status?: string;
    Fk_IdUser?: number;
    DateRecords?: string;
    Delete?: string;
    Fk_IdUserUpdate?: number;
    DateUpdate?: string;
    token?: string;
}