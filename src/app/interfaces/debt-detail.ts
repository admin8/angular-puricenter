export interface DebtDetail {
    Id_Detail?: number;
    Date?: string;
    Note?: string;
    Quantity?: number;
    Price?: number;
    Total?: string;
    User?: string;
    DateRecords?: string;
    Route?: string;
    Name?: string;
}