export interface CellSheet {
    Id_Cell?:number;
    Fk_IdPeriod:Number;
    Col:Number;
    Value?:Number;
    Price?:Number;
    Delete?:Number;
    TypeCell?:string;
    CellDisabled?:string;
    Type:string;
    CellColspan?:string;
    CellClass?:string;
}
