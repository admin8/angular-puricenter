export interface User {
    id?: number;
    access_token?: string;
    type?: string;
    expires_in?: number;
    name?: string;
    user?: string;
    token_type?: string;
    email?: string;
    route?: string;
    Fk_IdRoute?: string;
    UserType?: string;
    Active?: string;
    token?: string;
}
