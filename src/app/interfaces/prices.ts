export interface Prices {
    Id_Product?: number;
    Price?: string;
    Route?: string;
    Description?: string;
}