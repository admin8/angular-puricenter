export interface Products {
    Id_Product: number;
    Fk_IdUsuario?: number;
    Product: string;
    Price: string;
    Description?: string;
    Fk_IdRoute: number;
    DateRecords?: string;
    DateUpdate?: string;
    Fk_IdUserUpdate?: string;
    Delete: number;
    Active: number;
}