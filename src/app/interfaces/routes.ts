export interface RoutesSales {
    Id_Route: number;
    Route: string;
    DateRecords?: string;
}