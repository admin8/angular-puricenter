export interface Debt {
    Id_Detail?: number;
    Expense?: string;
    Cost?: number;
    Total?: string;
    Route?: string;
}