export interface Debt {
    Id_Client?: number;
    Client?: string;
    Quantity?: number;
    Total?: string;
    Route?: string;
    Price?: number;
}