export interface Employes {
    Id_Employe?: number;
    Name?: string;
    Position?: string;
    Salary?: number;
    Delete?: number;
    Active?: number;
    Route?: string;
    Fk_IdRoute?: number;
    Fk_IdUserRecords?: number;
    Fk_IdUserUpdate?: number;
    DateRecords?: string;
    DateUpdate?: string;
    token?: string;
    Fk_IdPeriod?: number;
}
