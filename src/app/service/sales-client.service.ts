import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import urljoin from 'url-join';
@Injectable({
  providedIn: 'root'
})
export class SalesClientService {
  url     = environment.apiUrl;
  headers = new HttpHeaders({'Content-Type': 'application/json'});
  token   =  localStorage.getItem('token');
  constructor( private http: HttpClient) {
    this.url =  urljoin(this.url, 'sales-client');
  }
  get($order?: any, $paginator?: any, $search?: any) {
    let url = this.url;
    if (Object.entries($order).length) {
      const $direction = $order.direction ? $order.direction : 'asc';
      url = urljoin(this.url, 'order', $order.active, $direction);
    }
    if (Object.entries($paginator).length) {
      const page = $paginator.pageIndex.toString();
      const size = $paginator.pageSize.toString();
      url = urljoin(url, 'page', page, size);
    }
    const client = $search.Client ? $search.Client : '-1';
    const ByBill = $search.ByBill ? $search.ByBill : '-1';
    const route  = $search.Route ? $search.Route   : '-1';
    url = urljoin(url, 'client', client, 'bybill', ByBill, 'route', route, 'token', this.token);
    return this.http.get(url);
  }
  getDetail($IdClient: string, $order?: any, $paginator?: any, $search?: any) {
    let url = urljoin(this.url, 'detail', $IdClient);
    if (Object.entries($order).length) {
      const $direction = $order.direction ? $order.direction : 'asc';
      url = urljoin(url, 'order', $order.active, $direction);
    }
    if (Object.entries($paginator).length) {
      const page = $paginator.pageIndex.toString();
      const size = $paginator.pageSize.toString();
      url = urljoin(url, 'page', page, size);
    }
    const quantity    = $search.Quantity ? $search.Quantity : '-1';
    const price       = $search.Price ? $search.Price : '-1';
    const note        = $search.Note ? $search.Note : '-1';
    const route       = $search.Route ? $search.Route : '-1';
    const total       = $search.Total ? $search.Total : '-1';
    const date        = $search.Date ? $search.Date : '-1';
    const user        = $search.User ? $search.User : '-1';
    const BillNumber  = $search.BillNumber ? $search.BillNumber : '-1';
    const RecordsBill = $search.RecordsBill ? $search.RecordsBill : '-1';
    const BillDate    = $search.BillDate ? $search.BillDate : '-1';
    const UserBill    = $search.UserBill ? $search.UserBill : '-1';
    const DateInin    = $search.DateInin ? $search.DateInin : '-1';
    const DateEnd     = $search.DateEnd ? $search.DateEnd : '-1';
    const allNotes    = $search.ViewAllNotes ? $search.ViewAllNotes : -1;

    url = urljoin(url, 'note', note, 'quantity', quantity, 'price', price, 'total', total, 'date', date,
                  'route', route, 'user', user, 'billnumber', BillNumber, 'recordsbill', RecordsBill,
                  'BillDate', BillDate, 'UserBill', UserBill, 'DateInin', DateInin, 'DateEnd', DateEnd,
                  'allNotes', `${allNotes}`, 'token', this.token);
    return this.http.get(url);
  }
  saveBill(data) {
    data.token = this.token;
    return this.http.post(urljoin(this.url, 'save'), data, {headers: this.headers});
  }
  delete( data ) {
    data.token = this.token;
    return this.http.put(urljoin(this.url, 'restore-notes'), data, {headers: this.headers});
  }
}
