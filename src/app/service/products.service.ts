import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import urljoin from 'url-join';
import { Prices } from '../interfaces/prices';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  url = environment.apiUrl;
  headers =  new HttpHeaders({'Content-Type': 'application/json'});
  token = localStorage.getItem('token');
  constructor(private http: HttpClient) {
    this.url = urljoin(this.url, 'products');
  }
  get($FkIdRoute: number) {
    const $url = urljoin( this.url, 'route', `${$FkIdRoute}`, 'token', this.token );
    return this.http.get($url);
  }
  getAll($order?: any, $paginator?: any, $search?: Prices) {
    let url = urljoin(this.url, 'all');
    if (Object.entries($order).length) {
      const $direction = $order.direction ? $order.direction: 'asc';
      url = urljoin(url, 'order', $order.active, $direction);
    }
    if (Object.entries($paginator).length) {
      const page = $paginator.pageIndex.toString();
      const size = $paginator.pageSize.toString();
      url = urljoin(url, 'page', page, size);
    }
    const price       = $search.Price ? $search.Price : '-1';
    const route       = $search.Route ? $search.Route : '-1';
    const description = $search.Description ? $search.Description : '-1';
    url = urljoin(url, 'price', price, 'route', route, 'description', description, 'token', this.token);
    return this.http.get(url);
  }
  save($data: any) {
    $data.token = this.token;
    return this.http.post(urljoin(this.url, 'new'), $data, { headers: this.headers});
  }
  delete(id: number) {
    return this.http.delete(urljoin(this.url, 'delete', `${id}`, 'token', this.token));
  }
}


