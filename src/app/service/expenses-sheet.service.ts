import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import urljoin from 'url-join';
@Injectable({
  providedIn: 'root'
})
export class ExpensesSheetService {
  url = environment.apiUrl;
  headers = new HttpHeaders({'Content-Type': 'application/json'});
  token = localStorage.getItem('token');
  constructor(private http: HttpClient) {
    this.url =  urljoin(this.url, 'expenses-sheet');
  }
  get($FkIdPeriod: any) {
    const $url = urljoin(this.url, '/period/', `${$FkIdPeriod}`, 'token', this.token);
    return this.http.get($url);
  }
  save($data: any ) {
    const $url = urljoin(this.url, 'save-expense');
    $data.token = this.token;
    return this.http.post($url, $data, { headers: this.headers});
  }
  delete($IdDetail: number) {
    const urlDelete = urljoin(this.url, 'delete', `${$IdDetail}`, 'token', this.token);
    return this.http.delete(urlDelete);
  }
}
