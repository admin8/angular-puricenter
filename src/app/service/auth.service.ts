import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import urljoin from 'url-join';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { User } from '../interfaces/user';
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  url = environment.apiUrl;
  headers = new HttpHeaders({'Content-Type': 'application/json'});
  currentUser?: User;
  constructor(private http: HttpClient, private router: Router) {
    this.url =  urljoin(this.url, 'auth');
    if (this.isLoggedIn()) {
      this.currentUser = JSON.parse(localStorage.getItem('user'));
    }
  }
  login( $credentials: any ) {
    return this.http
            .post( urljoin(this.url, 'login' ), $credentials, {headers: this.headers} );
  }
  saveToken($user: User) {
    this.currentUser =  $user;
    localStorage.setItem('token', $user.access_token);
    localStorage.setItem('route', $user.route);
    localStorage.setItem('user', JSON.stringify({name: $user.name, email: $user.user, type: $user.type, route: $user.route}));
    if ($user.type === 'Repartidor') {
      window.location.href = '/rutas/cortes';
    } else {
      window.location.href = '/';
    }
  }
  logout() {
    this.http
      .post( urljoin(this.url, 'logout' ), { token : localStorage.getItem('token')}, {headers: this.headers} )
      .subscribe( (res: any) => console.log( res ) );
    localStorage.clear();
    this.currentUser = null;
    this.router.navigate(['/login']);
  }
  isLoggedIn() {
    return localStorage.getItem('token') !== null;
  }
}