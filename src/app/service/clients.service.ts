import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import urljoin from 'url-join';
import { from, Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SearchClients } from '../catalogos/clientes/search-clients';
import { Clients } from '../catalogos/clientes/clients';

@Injectable({
  providedIn: 'root'
})
export class ClientsService {
  url = environment.apiUrl;
  headers = new HttpHeaders({'Content-Type':'application/json'});
  token = localStorage.getItem('token');
  constructor(private http: HttpClient) {
    this.url =  urljoin(this.url, 'clients');
  }
  get($order?: any, $paginator?: any, $search?: SearchClients){
    let url = this.url;
    if (Object.entries($order).length) {
      const $direction = $order.direction ? $order.direction: 'asc';
      url = urljoin(this.url, 'order', $order.active, $direction);
    }
    if (Object.entries($paginator).length) {
      const page = $paginator.pageIndex.toString();
      const size = $paginator.pageSize.toString();
      url = urljoin(url, 'page', page, size);
    }
    const client  = $search.client ? $search.client : '-1';
    const phone   = $search.phone ? $search.phone : '-1';
    const address = $search.address ? $search.address : '-1';
    const route   = $search.route ? $search.route : '-1';
    url = urljoin(url, 'client', client, 'phone', phone, 'address', address, 'route', route, 'token', this.token);
    return this.http.get(url);
  }
  getAll($FkIdRoute: number) {
    return this.http.get(urljoin(this.url, 'dropdawn', `${$FkIdRoute}`, 'token', this.token));
  }
  getById($Id: number) {
    return this.http.get(urljoin(this.url, 'one', $Id, 'token', this.token));
  }
  delete($IdClient: number): Observable<any> {
    let urlClient = urljoin(this.url, 'delete');
    urlClient = urlClient + '/' + $IdClient;
    return this.http.delete(urljoin(urlClient, 'token', this.token));
  }
  save($Client: Clients) {
    $Client.token = this.token;
    return this.http.post(urljoin(this.url, 'new'), $Client, { headers: this.headers});
  }
  edit($IdClient, $Client: Clients) {
    $Client.token = this.token;
    return this.http.put(urljoin(this.url, 'edit', `${$IdClient}`), $Client, { headers: this.headers});
  }
}