import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import urljoin from 'url-join'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Expenses } from '../interfaces/expenses';
@Injectable({
  providedIn: 'root'
})
export class ExpensesService {
  url = environment.apiUrl;
  headers = new HttpHeaders({'Content-Type': 'application/json'});
  token = localStorage.getItem('token');
  constructor(private http: HttpClient) {
    this.url = urljoin(environment.apiUrl, 'expenses');
  }
  getAll($order?: any, $paginator?: any, $search?: Expenses) {
    let url = urljoin(this.url, 'all');
    if (Object.entries($order).length) {
      const $direction = $order.direction ? $order.direction: 'asc';
      url = urljoin(url, 'order', $order.active, $direction);
    }
    if (Object.entries($paginator).length) {
      const page = $paginator.pageIndex.toString();
      const size = $paginator.pageSize.toString();
      url = urljoin(url, 'page', page, size);
    }
    const expense       = $search.Expense ? $search.Expense : '-1';
    url = urljoin(url, 'expense', expense, 'token', this.token);
    return this.http.get(url);
  }
  dropdawn() {
    return this.http.get(urljoin(this.url, 'dropdown', 'token', this.token));
  }
  save(expense: Expenses) {
    expense.token = this.token;
    return this.http.post(urljoin(this.url, 'new'), expense, {headers: this.headers});
  }
  delete(id: number) {
    return this.http.delete(urljoin(this.url, 'delete', `${id}`, 'token', this.token));
  }
}
