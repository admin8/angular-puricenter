import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import urljoin from 'url-join';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Employes } from '../interfaces/employes';
import { SearchEmployes } from '../interfaces/search-employes';

@Injectable({
  providedIn: 'root'
})
export class EmployesService {
  url = environment.apiUrl;
  headers = new HttpHeaders({'Content-Type': 'application/json'});
  token = localStorage.getItem('token');
  constructor(private http: HttpClient) {
    this.url = urljoin(environment.apiUrl, 'employes');
  }
  get($order?: any, $paginator?: any, $search?: SearchEmployes){
    let url = this.url;
    if (Object.entries($order).length) {
      const $direction = $order.direction ? $order.direction: 'asc';
      url = urljoin(this.url, 'order', $order.active, $direction);
    }
    if (Object.entries($paginator).length) {
      const page = $paginator.pageIndex.toString();
      const size = $paginator.pageSize.toString();
      url = urljoin(url, 'page', page, size);
    }
    const name     = $search.Name ? $search.Name : '-1';
    const position = $search.Position ? $search.Position : '-1';
    const salary   = $search.Salary ? $search.Salary : '-1';
    const active   = $search.Active ? $search.Active : '-1';
    const route    = $search.Route ? $search.Route : '-1';
    url = urljoin(url, 'name', name, 'position', position, 'salary', salary, 'route', route, 'active', active, 'token', this.token);
    return this.http.get(url);
  }
  getOne($IdEmploye: number) {
    return this.http.get(urljoin(this.url, 'one', $IdEmploye, 'token', this.token));
  }
  save(employes: Employes) {
    employes.token = this.token;
    return this.http.post(urljoin(this.url, 'new'), employes, {headers: this.headers});
  }
  edit(employes: Employes, $IdEmploye: number) {
    employes.token = this.token;
    return this.http.put(urljoin(this.url, 'edit', `${$IdEmploye}`), employes, {headers: this.headers});
  }
  delete(id: number) {
    return this.http.delete(urljoin(this.url, 'delete', `${id}`, 'token', this.token));
  }
  getSalary($FkIdPeriod: string) {
    return this.http.get(urljoin(this.url, 'salaris', `${$FkIdPeriod}`, 'token', this.token));
  }
  saveSalary(employes: Employes) {
    employes.token = this.token;
    return this.http.post(urljoin(this.url, 'salary'), employes, {headers: this.headers});
  }
}
