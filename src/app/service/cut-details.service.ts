import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import urljoin from 'url-join';

@Injectable({
  providedIn: 'root'
})
export class CutDetailsService {
  url = environment.apiUrl;
  headers = new HttpHeaders({'Content-Type': 'application/json'});
  token = localStorage.getItem('token');
  constructor(private http: HttpClient) {
    this.url =  urljoin(this.url, 'cut-details');
  }
  get($FkIdCut: any) {
    const $url = urljoin(this.url, '/cut/', `${$FkIdCut}`, 'token', this.token);
    return this.http.get($url);
  }
  getDebt($FkIdCut: any) {
    const $url = urljoin(this.url, '/debt/cut/', `${$FkIdCut}`, 'token', this.token);
    return this.http.get($url);
  }
  getPayment($FkIdCut: any) {
    const $url = urljoin(this.url, '/payment/cut/', `${$FkIdCut}`, 'token', this.token);
    return this.http.get($url);
  }
  saveSaleDay($data: any ) {
    const $url = urljoin(this.url, 'save-sale-day');
    $data.token = this.token;
    return this.http.post($url, $data, { headers: this.headers});
  }
  delete($IdDetail: number) {
    const urlDelete = urljoin(this.url, 'delete', `${$IdDetail}`, 'token', this.token);
    return this.http.delete(urlDelete);
  }
  deletePayment($IdDetail: number) {
    const urlDelete = urljoin(this.url, 'delete-payment', `${$IdDetail}`, 'token', this.token);
    return this.http.delete(urlDelete);
  }
  getNoteDebt($note: string) {
    const $url = urljoin(this.url, 'load-note');
    return this.http.post($url, { note : $note, token: this.token }, { headers : this.headers });
  }
  savePaymentDay($data: any ) {
    const $url = urljoin(this.url, 'save-payment-day');
    $data.token = localStorage.getItem('token');
    return this.http.post($url, $data, { headers: this.headers});
  }
}
