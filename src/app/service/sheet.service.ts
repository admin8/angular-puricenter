import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import urljoin from 'url-join'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CellSheet } from '../interfaces/cell-sheet';
@Injectable({
  providedIn: 'root'
})
export class SheetService {
  url = environment.apiUrl;
  headers = new HttpHeaders({'Content-Type': 'application/json'});
  token = localStorage.getItem('token');

  constructor( private http: HttpClient ) {
    this.url = urljoin(environment.apiUrl,'sheet'); 
  }
  get( $Fk_IdPeriod: number, $type: string ) {
    let _url = urljoin(this.url, 'all', 'id', `${$Fk_IdPeriod}`, 'type', `${$type}`, 'token', this.token);
    return this.http.get(_url);
  }
  saveValueCell( $cell: CellSheet ) {
    return this.http.post(urljoin(this.url, 'save-cell'),
                  { Id_Cell: $cell.Id_Cell, Value: $cell.Value, token: this.token },
                  {headers: this.headers});
  }
  saveValueCellTotales( $cell: CellSheet ) {
    return this.http.post(urljoin(this.url, 'save-cell-totales'),
                  { data: $cell , token: this.token },
                  {headers: this.headers});
  }
}