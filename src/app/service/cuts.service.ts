import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import urljoin from 'url-join';

@Injectable({
  providedIn: 'root'
})
export class CutsService {
  url = environment.apiUrl;
  headers = new HttpHeaders({'Content-Type': 'application/json'});
  constructor(private http: HttpClient) {
    this.url =  urljoin(this.url, 'cuts');
  }
  get($FkIdPeriod: any) {
    const $url = urljoin(this.url, '/period/', `${$FkIdPeriod}`);
    return this.http.get($url);
  }
}
