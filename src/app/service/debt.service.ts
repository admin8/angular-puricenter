import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import urljoin from 'url-join';
import { Debt } from '../interfaces/debt';
import { DebtDetail } from '../interfaces/debt-detail';
@Injectable({
  providedIn: 'root'
})
export class DebtService {
  url = environment.apiUrl;
  headers = new HttpHeaders({'Content-Type': 'application/json'});
  token =  localStorage.getItem('token');
  constructor( private http: HttpClient) {
    this.url =  urljoin(this.url, 'debt');
  }
  get($order?: any, $paginator?: any, $search?: any) {
    let url = this.url;
    if (Object.entries($order).length) {
      const $direction = $order.direction ? $order.direction: 'asc';
      url = urljoin(this.url, 'order', $order.active, $direction);
    }
    if (Object.entries($paginator).length) {
      const page = $paginator.pageIndex.toString();
      const size = $paginator.pageSize.toString();
      url = urljoin(url, 'page', page, size);
    }
    const client   = $search.Client ? $search.Client : '-1';
    const quantity = $search.Quantity ? $search.Quantity : '-1';
    const price    = $search.Price ? $search.Price : '-1';
    const route    = $search.Route ? $search.Route : '-1';
    url = urljoin(url, 'client', client, 'quantity', quantity, 'price', price, 'route', route, 'token', this.token);
    return this.http.get(url);
  }
  getDetail($Id_Client: string, $order?: any, $paginator?: any, $search?: DebtDetail) {
    let url = urljoin(this.url, 'detail', $Id_Client);
    if (Object.entries($order).length) {
      const $direction = $order.direction ? $order.direction: 'asc';
      url = urljoin(url, 'order', $order.active, $direction);
    }
    if (Object.entries($paginator).length) {
      const page = $paginator.pageIndex.toString();
      const size = $paginator.pageSize.toString();
      url = urljoin(url, 'page', page, size);
    }
    const quantity   = $search.Quantity ? $search.Quantity : '-1';
    const price = $search.Price ? $search.Price : '-1';
    const note = $search.Note ? $search.Note : '-1';
    const route   = $search.Route ? $search.Route : '-1';
    const total   = $search.Total ? $search.Total : '-1';
    const date   = $search.Date ? $search.Date : '-1';
    const user   = $search.User ? $search.User : '-1';
    const record   = $search.DateRecords ? $search.DateRecords : '-1';
    url = urljoin(url, 'note', note, 'quantity', quantity, 'price', price, 'total', total, 'date', date,
                  'route', route, 'user', user, 'record', record, 'token', this.token);
    return this.http.get(url);
  }
}
