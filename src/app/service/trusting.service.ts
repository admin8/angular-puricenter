import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import urljoin from 'url-join'
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TrustingService {
  url = environment.apiUrl;
  headers = new HttpHeaders({'Content-Type': 'application/json'});
  token = localStorage.getItem('token');
  constructor(private http: HttpClient) {
    this.url = urljoin(environment.apiUrl, 'trusting');
  }
  get($type: number){
    let _url = urljoin(this.url, 'all', 'type', `${$type}`);
    return this.http.get(_url);
  }
  getTrustPeriod($type: string, $FkIdPeriod: number, $Status: string) {
    return this.http.get(urljoin(this.url, 'period', `${$FkIdPeriod}`, 'type', `${$type}`, 'status', $Status, 'token', this.token));
  }
  getNoteForPayment($note: string) {
    return this.http.get(urljoin(this.url, 'note', `${$note}`, 'token', this.token));
  }
  save(trustings: any) {
    trustings.token = this.token;
    return this.http.post(urljoin(this.url, 'new'), trustings, {headers: this.headers});
  }
  savePayment($payment: any) {
    $payment.token = this.token;
    return this.http.post(urljoin(this.url, 'payment'), $payment, {headers: this.headers});
  }
  delete(id: number) {
    return this.http.delete(urljoin(this.url, 'delete', `${id}`, 'token', this.token));
  }
  deletePayment(id: number) {
    return this.http.delete(urljoin(this.url, 'delete-payment', `${id}`, 'token', this.token));
  }
  getAllTrust($order?: any, $paginator?: any, $search?: any) {
    let url = urljoin(this.url, 'all');
    if (Object.entries($order).length) {
      const $direction = $order.direction ? $order.direction: 'asc';
      url = urljoin(url, 'order', $order.active, $direction);
    }
    if (Object.entries($paginator).length) {
      const page = $paginator.pageIndex.toString();
      const size = $paginator.pageSize.toString();
      url = urljoin(url, 'page', page, size);
    }
    const client  = $search.Client ? $search.Client : '-1';
    const quantity   = $search.Quantity ? $search.Quantity : '-1';
    const total = $search.Total ? $search.Total : '-1';
    const route   = $search.Route ? $search.Route : '-1';
    url = urljoin(url, 'client', client, 'quantity', quantity, 'total', total, 'route', route, 'token', this.token);
    return this.http.get(url);
  }
  getDetail($Id_Client: string, $order?: any, $paginator?: any, $search?: any) {
    let url = urljoin(this.url, 'detail', $Id_Client);
    if (Object.entries($order).length){
      const $direction = $order.direction ? $order.direction: 'asc';
      url = urljoin(url, 'order', $order.active, $direction);
    }
    if (Object.entries($paginator).length) {
      const page = $paginator.pageIndex.toString();
      const size = $paginator.pageSize.toString();
      url = urljoin(url, 'page', page, size);
    }
    const quantity = $search.Quantity ? $search.Quantity : '-1';
    const price    = $search.Price ? $search.Price : '-1';
    const note     = $search.Note ? $search.Note : '-1';
    const route    = $search.Route ? $search.Route : '-1';
    const total    = $search.Total ? $search.Total : '-1';
    const date     = $search.Date ? $search.Date : '-1';
    const user     = $search.User ? $search.User : '-1';
    const record   = $search.DateRecords ? $search.DateRecords : '-1';
    url = urljoin(url, 'note', note, 'quantity', quantity, 'price', price, 'total', total,
                'date', date, 'route', route, 'user', user, 'record', record, 'token', this.token);
    return this.http.get(url);
  }

}

