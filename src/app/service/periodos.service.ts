import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import urljoin from 'url-join';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Periodos } from '../interfaces/periodos';
@Injectable({
  providedIn: 'root'
})
export class PeriodosService {
  url = environment.apiUrl;
  headers = new HttpHeaders({'Content-Type': 'application/json'});
  token = localStorage.getItem('token');
  constructor(private http: HttpClient) {
    this.url = urljoin(environment.apiUrl, 'periods');
  }
  get($status= 'all') {
    return this.http.get(urljoin(this.url, 'all', 'status', $status, 'token', this.token));
  }
  save(period: Periodos) {
    period.token = this.token;
    return this.http.post(urljoin(this.url, 'new'), period, {headers: this.headers});
  }
  delete(id: number) {
    return this.http.delete(urljoin(this.url, 'delete', `${id}`, 'token', this.token));
  }
  open(id: number) {
    return this.http.put(urljoin(this.url, 'open'), { 'Id_Period': id, token: this.token}, {headers: this.headers});
  }
  close(id: number) {
    return this.http.put(urljoin(this.url, 'close'), { 'Id_Period': id, token: this.token}, {headers: this.headers});
  }
}
