import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { Router, CanActivate, ActivatedRouteSnapshot } from '@angular/router';
import decode from 'jwt-decode';

@Injectable({
  providedIn: 'root'
})
export class RoleGuardService implements CanActivate {
  constructor( private auth: AuthService, private router: Router) {

  }
  canActivate(route: ActivatedRouteSnapshot): boolean {
    // this will be passed from the route config
    // on the data property
    const role = route.data.role;
    const user  = JSON.parse(localStorage.getItem('user'));
    if (
      !this.auth.isLoggedIn() || (
        role !== user.type && user.type !== 'Super usuario')
    ) {
      this.auth.logout();
      return false;
    }
    return true;
  }
}
