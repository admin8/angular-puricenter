import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import urljoin from 'url-join';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../interfaces/user';
@Injectable({
  providedIn: 'root'
})
export class UserService {
  url = environment.apiUrl;
  headers = new HttpHeaders({'Content-Type': 'application/json'});
  token = localStorage.getItem('token');
  
  constructor(private http: HttpClient) {
    this.url = urljoin(environment.apiUrl, 'users');
  }
  get($order?: any, $paginator?: any, $search?: any) {
    let url = this.url;
    if (Object.entries($order).length) {
      const $direction = $order.direction ? $order.direction: 'asc';
      url = urljoin(this.url, 'order', $order.active, $direction);
    }
    if (Object.entries($paginator).length) {
      const page = $paginator.pageIndex.toString();
      const size = $paginator.pageSize.toString();
      url = urljoin(url, 'page', page, size);
    }

    const name   = $search.name ? $search.name : '-1';
    const user   = $search.user ? $search.user : '-1';
    const email  = $search.email ? $search.email : '-1';
    const route  = $search.Route ? $search.Route : '-1';
    const type   = $search.UserType ? $search.UserType : '-1';
    const active = $search.Active ? $search.Active : '-1';

    url = urljoin(url, 'name', name, 'user', user, 'email', email, 'route', route, 'type', type, 'active', active, 'token', this.token);
    return this.http.get(url);
  }
  getOne($IdUser: number) {
    return this.http.get(urljoin(this.url, 'one', $IdUser, 'token', this.token));
  }
  save(data: User) {
    data.token = this.token;
    return this.http.post(urljoin(this.url, 'new'), data, {headers: this.headers});
  }
  edit(data: User, $IdUser: number) {
    data.token = this.token;
    return this.http.put(urljoin(this.url, 'edit', `${$IdUser}`), data, {headers: this.headers});
  }
  delete(id: number) {
    return this.http.delete(urljoin(this.url, 'delete', `${id}`, 'token', this.token));
  }
}
