import { Component, OnInit } from '@angular/core';
import { PeriodosService } from 'src/app/service/periodos.service';
import { MatDialog, MatSnackBar } from '@angular/material';
import { DateFormatPipe } from 'src/app/pipes/date-format.pipe';
import { TrustingService } from 'src/app/service/trusting.service';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Periodos } from 'src/app/interfaces/periodos';
import { CutsService } from 'src/app/service/cuts.service';
import { Cuts } from 'src/app/interfaces/cuts';
import { Clients } from 'src/app/catalogos/clientes/clients';
import { ClientsService } from 'src/app/service/clients.service';
import { ProductsService } from 'src/app/service/products.service';
import { Products } from 'src/app/interfaces/products';
import { CutDetailsService } from 'src/app/service/cut-details.service';
import { NotifierService } from 'angular-notifier';
import { DialogComponent } from '../../partials/dialog/dialog.component';
import { AuthService } from 'src/app/service/auth.service';
import { ExpensesDetailsService } from 'src/app/service/expenses-details.service';
import { Expenses } from 'src/app/interfaces/expenses';
import { ExpensesService } from 'src/app/service/expenses';

@Component({
  selector: 'app-cortes',
  templateUrl: './cortes.component.html',
  styleUrls: ['./cortes.component.css'],
  providers: [DateFormatPipe]
})
export class CortesComponent implements OnInit {
  private notifier: NotifierService;

  form: FormGroup;
  formAddSales: FormGroup;
  formDebtDay: FormGroup;
  formPaymentDay: FormGroup;
  formExpenseDay: FormGroup;
  periods: Periodos[];
  clients: Clients[];
  expenses: Expenses[];
  periodSelect: any;
  days: Cuts;
  FkIdRoute: any = localStorage.getItem('route');
  products: any;
  isLoadingResults: any =  true;
  addTotalSaleDay: any;
  addTotalDebtDay: any;
  addTotalPaymentDay: any;
  $price: number;
  WorkDayM = 0;
  $tableSalesDay: any;
  $tableDebtDay: any;
  $tablePaymentDay: any;
  $tableExpensesDay: any;
  ClientNote: string;
  PriceNote: string;
  QuantityNote: string;
  PromotionNote: string;
  TotalNote: number;
  TotalSalesDay = 0;
  TotalGarrafonesSalesDay = 0;
  TotalGarrafonesDebtDay = 0;
  TotalDebtDay = 0;
  TotalPaymentDay = 0;
  TotalGarrafonesPaymentDay = 0;
  TotalExpensesDay = 0;
  TotalSalesCosala = 0;
  TotalSalesAjijic = 0;

  constructor(private periodService: PeriodosService,
              private clientService: ClientsService,
              private productService: ProductsService,
              private cutDetailsService: CutDetailsService,
              private cutsService: CutsService,
              public dialog: MatDialog,
              public dateFormat: DateFormatPipe,
              public snack: MatSnackBar,
              public formBuilder: FormBuilder,
              private trusting: TrustingService,
              private auth: AuthService,
              private expenseDetailService: ExpensesDetailsService,
              private expenseService: ExpensesService,
              notifier: NotifierService)
  {
    this.notifier = notifier;
    this.form =  new FormGroup({ WorkWeek: new FormControl(null), WorkDay: new FormControl(null) } );
    this.formAddSales =  this.formBuilder.group({
      Fk_IdClient: ['', Validators.required],
      Fk_IdProduct: ['', [Validators.required]],
      Quantity: ['', [Validators.required]],
      Total: ['', [Validators.required]],
      Note: ['', [Validators.required]],
      Promotion: ['', []],
    });
    this.formDebtDay =  this.formBuilder.group({
      Fk_IdClientDebt: ['', Validators.required],
      Fk_IdProductDebt: ['', [Validators.required]],
      QuantityDebt: ['', [Validators.required]],
      TotalDebt: ['', [Validators.required]],
      NoteDebt: ['', [Validators.required]],
      PromotionDebt: ['', []],
    });
    this.formPaymentDay =  this.formBuilder.group({
      ClientPayment: [{disabled: true}, []],
      PricePayment: [{disabled: true}, []],
      QuantityPayment: [{disabled: true}, []],
      TotalPayment: [{disabled: true}, []],
      NotePayment: ['', [Validators.required]],
      PromotionPayment: [{disabled: true}, []],
    });
    this.formExpenseDay =  this.formBuilder.group({
      Fk_IdExpense: ['',[Validators.required]],
      Cost: ['',[Validators.required]],
      Description: [],
    });
    //this load model for dropdawn with clients
    this.clientService.getAll(this.FkIdRoute)
        .subscribe ((res: Clients[]) => {
          this.clients = res;
        }, error => {
          console.log(error);
          if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
            this.auth.logout();
          }
        });
    this.productService.get(this.FkIdRoute)
        .subscribe ((res: Products[]) => {
          this.products = res;
        }, error => {
          console.log(error);
          if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
            this.auth.logout();
          }
        });
    this.expenseService.dropdawn()
      .subscribe( (res: Expenses[]) => {
        this.expenses = res;
      }, error => {
        console.log(error);
        if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
          this.auth.logout();
        }
      });
    this.loadPeriods();
  }

  ngOnInit() {
  }
  loadPeriods() {
    this.periodService.get('Abierto')
      .subscribe((data: Periodos[]) => {
        this.periods = data;
        this.isLoadingResults = false;
      }, error => {
        console.log(error);
        if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
          this.auth.logout();
        }
      });
  }
  onChangeWeek() {
    const $IdPeriod = this.form.value.WorkWeek;
    this.cutsService.get($IdPeriod)
        .subscribe((res: Cuts) => {
          this.days = res;
        }, error => {
          console.log(error);
          if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
            this.auth.logout();
          }
        });
  }
  onChangeDay() {
    this.isLoadingResults = true;
    this.loadTableSalesDay();
    this.loadTableDebtDay();
    this.loadTablePaymentDay();
    this.loadTableExpensesDay();
  }
  loadTableSalesDay() {
    const $IdCut = this.form.value.WorkDay;
    this.TotalSalesDay = 0;
    this.TotalGarrafonesSalesDay = 0;
    this.cutDetailsService.get($IdCut)
        .subscribe((res: any[]) => {
          this.$tableSalesDay = res;
          this.$tableSalesDay.forEach( row => {
            this.TotalSalesDay += (parseFloat(row.Quantity) * parseFloat(row.Price));
            this.TotalGarrafonesSalesDay += parseInt(row.Quantity);
          });
          this.isLoadingResults = false;
        }, error => {
          console.log(error);
          if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
            this.auth.logout();
          }
        });
  }
  loadTableDebtDay() {
    this.TotalGarrafonesDebtDay = 0;
    this.TotalDebtDay = 0;
    const $IdCut = this.form.value.WorkDay;
    this.cutDetailsService.getDebt($IdCut)
        .subscribe((res: any[]) => {
          this.$tableDebtDay = res;
          this.$tableDebtDay.forEach(row => {
            this.TotalDebtDay += (parseFloat(row.Quantity) * parseFloat(row.Price));
            this.TotalGarrafonesDebtDay += parseInt(row.Quantity);
          });
          this.isLoadingResults = false;
        }, error => {
          console.log(error);
          if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
            this.auth.logout();
          }
        });
  }
  loadTablePaymentDay() {
    this.TotalPaymentDay = 0;
    this.TotalGarrafonesPaymentDay = 0;
    const $IdCut = this.form.value.WorkDay;
    this.cutDetailsService.getPayment($IdCut)
        .subscribe((res: any[]) => {
          this.$tablePaymentDay = res;
          this.$tablePaymentDay.forEach(row => {
            this.TotalPaymentDay += (parseFloat(row.Quantity) * parseFloat(row.Price));
            this.TotalGarrafonesPaymentDay += parseInt(row.Quantity);
          });
          this.isLoadingResults = false;
        }, error => {
          console.log(error);
          if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
            this.auth.logout();
          }
        });
  }
  saveSaleDay() {
    if (this.formAddSales.valid) {
      const data = this.formAddSales.value;
      data.Fk_IdCut = this.form.value.WorkDay;
      data.Price = this.$price;
      data.Type = 'Venta del dia';
      this.cutDetailsService.saveSaleDay(data)
          .subscribe((res: any) => {
            if (res.result === 'success') {
              this.formAddSales.reset();
              this.loadTableSalesDay();
              this.notifier.notify( 'success', res.alert );
            } else {
              this.notifier.notify( 'error', res.alert );
            }
          }, error => {
            console.log(error);
            if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
              this.auth.logout();
            }
          });
    }
  }
  saveDebtDay() {
    if (this.formDebtDay.valid) {
      let data: any;
      data.Fk_IdCut = this.form.value.WorkDay;
      data.Price = this.$price;
      data.Type = 'Fiados del dia';
      data.Fk_IdClient = this.formDebtDay.value.Fk_IdClientDebt;
      data.Fk_IdProduct = this.formDebtDay.value.Fk_IdProductDebt;
      data.Note = this.formDebtDay.value.NoteDebt;
      data.Quantity = this.formDebtDay.value.QuantityDebt;
      data.Promotion = this.formDebtDay.value.PromotionDebt;
      this.cutDetailsService.saveSaleDay(data)
          .subscribe((res: any) => {
            if (res.result === 'success') {
              this.formDebtDay.reset();
              this.loadTableDebtDay();
              this.notifier.notify( 'success', res.alert );
            } else {
              this.notifier.notify( 'error', res.alert );
            }
          }, error => {
            console.log(error);
            if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
              this.auth.logout();
            }
          });
    }
  }
  savePaymentDay() {
    if (this.formPaymentDay.valid) {
      let data: any;
      data.Fk_IdCutPayment = this.form.value.WorkDay;
      data.Note = this.formPaymentDay.value.NotePayment;
      this.cutDetailsService.savePaymentDay( data )
          .subscribe((res: any) => {
            if ( res.result === 'success' ) {
              this.formPaymentDay.reset();
              this.loadTablePaymentDay();
              this.notifier.notify( 'success', res.alert );
            } else {
              this.notifier.notify( 'error', res.alert );
            }
          }, error => {
            console.log(error);
            if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
              this.auth.logout();
            }
          });
    }
  }
  calculaTotalAddSaleDay() {
    const Quantity = this.formAddSales.value.Quantity;
    const IdProduct = this.formAddSales.value.Fk_IdProduct;
    this.products.find( prod => {
      if ( prod.Id_Product === IdProduct ) {
        this.$price = Number(prod.Price);
      }
    });
    this.addTotalSaleDay = this.$price * Quantity;
  }
  calculaTotalAddDebtDay() {
    const Quantity = this.formDebtDay.value.QuantityDebt;
    const IdProduct = this.formDebtDay.value.Fk_IdProductDebt;
    this.products.find( prod => {
      if ( prod.Id_Product === IdProduct ) {
        this.$price = parseFloat(prod.Price);
      }
    });
    this.addTotalDebtDay = this.$price * Quantity;
  }
  deleteSaleDayDialog($IdDetail: number, $Client: string, $Quantity: string) {
    const dialogRef = this.dialog.open(DialogComponent, {
      minWidth: '350px',
      data: {title: `Eliminar venta de ${$Client}`, content: `¿Desea eliminar la venta del cliente ${$Client} de ${$Quantity} garrafones ?`}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === 'confirm') {
        this.cutDetailsService.delete($IdDetail)
            .subscribe((res: any) => {
              this.loadTableSalesDay();
              this.notifier.notify( 'info', res.alert );
            }, error => {
              console.log(error);
              if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
                this.auth.logout();
              }
            });
      }
    });
  }
  deleteDebtDayDialog($IdDetail: number, $Client: string, $Quantity: string) {
    const dialogRef = this.dialog.open(DialogComponent, {
      minWidth: '350px',
      data: {title: `Eliminar los fiados de ${$Client}`, content: `¿Desea eliminar los garrafones fiados del cliente ${$Client} de ${$Quantity} garrafones ?`}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === 'confirm') {
        this.cutDetailsService.delete($IdDetail)
            .subscribe((res: any) => {
              this.loadTableDebtDay()
              this.notifier.notify( 'info', res.alert );
            }, error => {
              console.log(error);
              if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
                this.auth.logout();
              }
            });
      }
    });
  }
  deletePaymentDayDialog($IdDetail: number, $Client: string, $Quantity: string) {
    const dialogRef = this.dialog.open(DialogComponent, {
      minWidth: '350px',
      data: {title: `Eliminar los pagos de ${$Client}`, content: `¿Desea eliminar el pago  del cliente ${$Client} de ${$Quantity} garrafones? OJO: solo se eliminara el pago más no la venta`}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === 'confirm') {
        this.cutDetailsService.deletePayment($IdDetail)
            .subscribe((res: any) => {
              this.loadTablePaymentDay();
              this.notifier.notify( 'info', res.alert );
            }, error => {
              console.log(error);
              if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
                this.auth.logout();
              }
            });
      }
    });
  }

  // this funcions is for load notes of debt
  loadNoteDebt() {
    const $note =  this.formPaymentDay.value.NotePayment;
    this.cutDetailsService.getNoteDebt($note)
        .subscribe((res: any) => {
          this.ClientNote =  res.Client;
          this.PriceNote =  res.Price;
          this.QuantityNote =  res.Quantity;
          this.PromotionNote =  res.Promotion;
          const price = res.Quantity * res.Price;
          this.TotalNote = price ? price : 0;
        }, error => {
          console.log(error);
          if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
            this.auth.logout();
          }
        });
  }


  // ***************************** this funcions for tab Expenses ******************************
  deleteExpenseDayDialog($IdDetail: number, $Expense: string, $Cost) {
    const dialogRef = this.dialog.open(DialogComponent, {
      minWidth: '350px',
      data: {title: `Eliminar el gasto ${$Expense}`, content: `¿Desea eliminar el gasto por ${$Expense} de ${$Cost}?`}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === 'confirm') {
        this.expenseDetailService.delete($IdDetail)
            .subscribe((res: any) => {
              this.loadTableExpensesDay();
              this.notifier.notify( 'info', res.alert );
            }, error => {
              console.log(error);
              if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
                this.auth.logout();
              }
            });
      }
    });
  }
  loadTableExpensesDay() {
    this.TotalExpensesDay = 0;
    const $IdCut = this.form.value.WorkDay;
    this.expenseDetailService.get($IdCut)
        .subscribe((res: any) => {
          this.$tableExpensesDay = res;
          this.$tableExpensesDay.forEach( row => {
            this.TotalExpensesDay += parseFloat(row.Cost);
          });
          this.isLoadingResults = false;
        }, error => {
          console.log(error);
          if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
            this.auth.logout();
          }
        });
  }
  saveExpenseDay() {
    if (this.formExpenseDay.valid) {
      let data = this.formExpenseDay.value;
      data.Fk_IdCut = this.form.value.WorkDay;
      this.expenseDetailService.save(data)
          .subscribe((res: any) => {
            if (res.result === 'success') {
              this.formExpenseDay.reset();
              this.loadTableExpensesDay();
              this.notifier.notify( 'success', res.alert );
            } else {
              this.notifier.notify( 'error', res.alert );
            }
          }, error => {
            console.log(error);
            if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
              this.auth.logout();
            }
          });
    }
  }
}
