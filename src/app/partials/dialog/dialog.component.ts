import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogConfig, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})
export class DialogComponent implements OnInit {
  title: string;
  content: string;
  constructor(public dialog: MatDialog, private dialogRef: MatDialogRef<DialogComponent>,
    @Inject(MAT_DIALOG_DATA) {title,content} ) {
      this.title =  title;
      this.content =  content;
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit() {
    this.dialogRef.updatePosition({top: '50px'});
  }

}
