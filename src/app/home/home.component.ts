import { Component, OnInit, ɵConsole} from '@angular/core';
import { PeriodosService } from '../service/periodos.service';
import { Periodos } from '../interfaces/periodos';
import { FormGroup, FormControl, FormBuilder, Validators, Form } from '@angular/forms';
import { MatDialog, MatDialogConfig, MatSnackBar } from '@angular/material';
import { DialogComponent } from '../partials/dialog/dialog.component';
import { DateFormatPipe } from '../pipes/date-format.pipe';
import { SheetService } from '../service/sheet.service';
import { TrustingService } from '../service/trusting.service';
import { ClientsDebt } from '../interfaces/clients-debt';
import { AuthService } from '../service/auth.service';
import { Alert } from 'selenium-webdriver';
import { NotifierService } from 'angular-notifier';
import { ClientsService } from '../service/clients.service';
import { ProductsService } from '../service/products.service';
import { Products } from '../interfaces/products';
import { Clients } from '../catalogos/clientes/clients';
import { RoutesSales } from '../interfaces/routes';
import { RoutesService } from '../service/routes.service';
import { Routes } from '@angular/router';
import { ExpensesService } from '../service/expenses';
import { Expenses } from '../interfaces/expenses';
import { ExpensesSheetService } from '../service/expenses-sheet.service';
import { Employes } from '../interfaces/employes';
import { EmployesService } from '../service/employes.service';
@Component({
// tslint:disable-next-line: indent
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.css'],
	providers: [DateFormatPipe]
})
export class HomeComponent implements OnInit {
	private notifier: NotifierService;
	isLoadingResults: any =  true;
	formTrustCosala: FormGroup;
	formTrustAjijic: FormGroup;
	formPaymentAjijic: FormGroup;
	formPaymentCosala: FormGroup;
	formExpenses: FormGroup;
	formSalary: FormGroup;
	table: any;
	tableAjijic: any;
	periods: Periodos[];
	form: FormGroup;
	statusPeriod: any ;
	selectedPeriod: any;
	periodSelect: any;
	tableTrustCosala: ClientsDebt[];
	tableTrustAjijic: ClientsDebt[];
	tableTrustPaymentCosala: ClientsDebt[];
	tableTrustPaymentAjijic: ClientsDebt[];
	tableEmployes: any;
	tableExpenses: any;
	displayedColumns: string[] = ['Options', 'Date', 'Client', 'Quantity', 'Price', 'Total'];
	noteTotalTrustCosala: number;
	noteTotalTrustAjijic: number;
	$price: number;
	clientsCosala: Clients[];
	productsCosala: any;
	clientsAjijic: Clients[];
	productsAjijic: any;
	minDate = new Date(2018, 0, 1);
  	maxDate = new Date(2020, 0, 1);
	notePayment: any = {Client:'', Price:'' ,Quantity:'', Total:'', Date:''};
	routes: RoutesSales[];
	expenses: Expenses[];

	totalSalaries: number= 0;
	TotalExpensesPeriod: number = 0;
	TotalPaymentAjijicPeriod: number = 0;
	TotalPayGarrafoneAjijicPeriod: number = 0;
	TotalPaymentChapalaPeriod: number = 0;
	TotalPayGarrafoneChapalaPeriod: number = 0;
	TotalTrustAjijicPeriod: number = 0;
	TotalTrustGarrafoneAjijicPeriod: number = 0;
	TotalTrustChapalaPeriod: number = 0;
	TotalTrustGarrafoneChapalaPeriod: number = 0;
	TotalSalesAjijic: number= 0;
	TotalSalesCosala: number= 0;
	TotalExpensesAjijic: number= 0;
	TotalExpensesCosala: number= 0;
	totalSalariesAjijic: number= 0;
	totalSalariesCosala: number= 0;
	constructor(private periodService: PeriodosService,
				public dialog: MatDialog,
				public dateFormat: DateFormatPipe,
				public snack: MatSnackBar,
				public formBuilder: FormBuilder,
				private sheetService: SheetService,
				private trusting: TrustingService,
				private auth: AuthService,
				notifier: NotifierService,
				private clientService: ClientsService,
				private productService: ProductsService,
				private routesServices: RoutesService,
				private expenseService: ExpensesService,
				private employeService: EmployesService,
				private expenseSheetService: ExpensesSheetService
	) {
		this.notifier = notifier;
		this.form =  new FormGroup({
			period: new FormControl(null)
		});
		this.formTrustCosala =  this.formBuilder.group({
			DateCosala: ['', Validators.required],
			Fk_IdClientCosala: ['', Validators.required],
			Fk_IdProductCosala: ['', [Validators.required]],
			QuantityCosala: ['', [Validators.required]],
			TotalCosala: ['', []],
			NoteCosala: ['', [Validators.required]],
		});
		this.formTrustAjijic =  this.formBuilder.group({
			DateAjijic: ['', Validators.required],
			Fk_IdClientAjijic: ['', Validators.required],
			Fk_IdProductAjijic: ['', [Validators.required]],
			QuantityAjijic: ['', [Validators.required]],
			TotalAjijic: ['', []],
			NoteAjijic: ['', [Validators.required]],
		});
		this.formPaymentCosala =  this.formBuilder.group({
			DatePaymentCosala: ['', Validators.required],
			NotePaymentCosala: ['', [Validators.required]],
			ClientPaymentCosala: [''],
			PricePaymentCosala: [''],
			QuantityPaymentCosala: [''],
			TotalPaymentCosala: [''],
			DateDebtPaymentCosala: [''],
		});
		this.formPaymentAjijic =  this.formBuilder.group({
			DatePaymentAjijic: ['', Validators.required],
			NotePaymentAjijic: ['', [Validators.required]],
			ClientPaymentAjijic: [''],
			PricePaymentAjijic: [''],
			QuantityPaymentAjijic: [''],
			TotalPaymentAjijic: [''],
			DateDebtPaymentAjijic: [''],
		});
		this.formExpenses =  this.formBuilder.group({
			Fk_IdRoute: ['',[Validators.required]],
			Fk_IdExpense: ['',[Validators.required]],
			Cost: ['',[Validators.required]],
			Description: [],
		  });
		this.loadPeriods();
		//this load model for dropdawn with clients
		this.clientService.getAll(1)//1=> cosala o chapala
			.subscribe ((res: Clients[]) => {
				this.clientsCosala = res;
			}, error => {
			console.log(error);
			if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
				this.auth.logout();
			}
        });
		this.productService.get(1)//1=> cosala o chapala
			.subscribe ((res: Products[]) => {
				this.productsCosala = res;
			}, error => {
			console.log(error);
			if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
				this.auth.logout();
			}
		});
		//this load model for dropdawn with clients
		this.clientService.getAll(2)//2=> ajijic
			.subscribe ((res: Clients[]) => {
				this.clientsAjijic = res;
			}, error => {
			console.log(error);
			if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
				this.auth.logout();
			}
        });
		this.productService.get(2)//2=> ajijic
			.subscribe ((res: Products[]) => {
				this.productsAjijic = res;
			}, error => {
			console.log(error);
			if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
				this.auth.logout();
			}
		});
		this.routesServices.get().subscribe ((res: RoutesSales[]) => {
				this.routes = res;
			}, error => {
				console.log(error);
				if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
					this.auth.logout();
				}
		});
		this.expenseService.dropdawn()
			.subscribe( (res: Expenses[]) => {
				this.expenses = res;
		}, error => {
			console.log(error);
			if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
			this.auth.logout();
			}
		});
  	}
  	ngOnInit() {
	}
	
	/**
	 * this function load periods to select
	 * esta funcion carga los periodos para el select
	 */
	loadPeriods() {
		this.periodService.get()
			.subscribe((data: Periodos[]) => {
				this.periods = data;
				this.isLoadingResults = false;
			}, error => {
				console.log(error);
				if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
					this.auth.logout();
				}
			});
	}
	/**
	 * this function is to change period select
	 * esta funcion es para cuando cambia el periodo en el select
	 */
	onChangePeriod() {
		this.TotalSalesCosala = 0;
		this.periodSelect = this.form.get('period').value;
		this.sheetService.get(this.periodSelect, 'cosala')
			.subscribe(
				(sheet: any) => {
					this.table = sheet;
					this.table.forEach(row => {
						row.find( cell => {
							if(cell.CellClass.includes('total-sale-cash-all')) {
								this.TotalSalesCosala += parseFloat(cell.Value) > 0 ? parseFloat(cell.Value) : 0;
							}
						})
					})
				}, error => {
					console.log(error);
					if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
						this.auth.logout();
					}
			});
		this.periods.filter(row => {
			if (JSON.stringify(row.Id_Period).includes(this.periodSelect)) {
				this.selectedPeriod = row;
				this.statusPeriod = row.Status;
				
				let dates: string;
				dates = `${row.DateInin}`;
				let year = parseInt(dates.split('-')[0]);
				let month = parseInt(dates.split('-')[1])-1;
				let day =  parseInt(dates.split('-')[2]);
				this.minDate = new Date( year, month, day);
				
				dates = row.DateEnd;
				year = parseInt(dates.split('-')[0]);
				month = parseInt(dates.split('-')[1])-1;
				day = parseInt(dates.split('-')[2]);
				this.maxDate = new Date(year, month, day);
			}
		});
		this.loadSheetAjijic();
		this.loadFiados('cosala');
		this.loadFiados('ajijic');
		this.loadPaymentTrusting('cosala');
		this.loadPaymentTrusting('ajijic');
		this.loadTableExpenses();
		this.loadTableExployes();
	}
	/**
	 * this function is to delete periods
	 * esta funcion es para eliminar periodos
	 */
	DeletePeriodDialog(): void {
		const dialogRef = this.dialog.open(DialogComponent, {
			minWidth: '350px',
			data: {title: 'Eliminar periodo', content: `¿Desea eliminar el periodo del ${this.dateFormat.transform(this.selectedPeriod.DateInin)} al ${this.dateFormat.transform(this.selectedPeriod.DateEnd)} ?`}
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result === 'confirm') {
				this.periodService.delete(this.selectedPeriod.Id_Period)
							.subscribe((res: any) => {
								this.periods = [];
								this.statusPeriod = null;
								this.table = {};
								this.snack.open(res.alert, 'Cerrar', { duration: 4000 });
								this.loadPeriods();
							}, error => {
								console.log(error);
								if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
									this.auth.logout();
								}
							});
			}
		});
	}
	/**
	 * this function is to open periods
	 * esta funcion es para abrir periodos
	 */
	OpenPeriodDialog(): void {
		const dialogRef = this.dialog.open(DialogComponent, {
			minWidth: '350px',
			data: {title: 'Abrir periodo', content: `¿Desea abrir el periodo del ${this.dateFormat.transform(this.selectedPeriod.DateInin)} al ${this.dateFormat.transform(this.selectedPeriod.DateEnd)} ?`}
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result === 'confirm') {
				this.periodService.open(this.selectedPeriod.Id_Period)
							.subscribe((res: any) => {
								this.periods = [];
								this.statusPeriod = 'Abierto';
								this.snack.open(res.alert, 'Cerrar', { duration: 4000 });
								this.loadPeriods();
							}, error => {
								console.log(error);
								if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
									this.auth.logout();
								}
							});
			}
		});
	}
	/**
	 * this function is to close periods
	 * esta funcion es para cerrar periodos
	 */
	ClosePeriodDialog(): void {
		const dialogRef = this.dialog.open(DialogComponent, {
			minWidth: '350px',
			data: {title: 'Cerrar periodo', content: `¿Desea cerrar el periodo del ${this.dateFormat.transform(this.selectedPeriod.DateInin)} al ${this.dateFormat.transform(this.selectedPeriod.DateEnd)} ?`}
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result === 'confirm') {
				this.periodService.close(this.selectedPeriod.Id_Period)
							.subscribe((res: any) => {
								this.periods = [];
								this.statusPeriod = 'Cerrado';
								this.snack.open(res.alert, 'Cerrar', { duration: 4000 });
								this.loadPeriods();
							}, error => {
								console.log(error);
								if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
									this.auth.logout();
								}
							});
			}
		});
	}
	loadSheet($event) {
		if (this.selectedPeriod) {
			if ($event.tab.textLabel === 'Ajijic') {
				this.loadSheetAjijic();
			} else if ($event.tab.textLabel === 'Fiados San Juan Cosala') {
				this.loadFiados('cosala');
			} else if ($event.tab.textLabel === 'Fiados Ajijic') {
				this.loadFiados('ajijic');
			} else if ($event.tab.textLabel === 'Pagados San Juan Cosala') {
				this.loadPaymentTrusting('cosala');
			} else if ($event.tab.textLabel === 'Pagados Ajijic') {
				this.loadPaymentTrusting('ajijic');
			} else if ($event.tab.textLabel === 'Gastos') {
				this.loadTableExpenses();
			}else if ($event.tab.textLabel === 'Sueldos') {
				this.loadTableExployes();
			}
		} else {
			this.snack.open('Favor de seleccionar un periodo!', 'Cerrar', { duration: 4000 });
		}
	}
	/**
	 ###########################################################################################################
	   ##################################    VENTA SAN JUAN COSALA     #######################################
	 ###########################################################################################################
	*/
	/**
	 * this function is to changed quantity in sales and send http request at server
	 * @param Id_Cell
	 * @param val
	 * @param row
	 */
	changed(Id_Cell: any, row: any) {
		let quantity = 0;
		let allQuantity = 0;
		let PriceForProduct = 0;
		let SumPriceForPlanta = 0;
		let SumTotalSales = 0;
		let price = 0;
		let pricePlant = false;
		this.TotalSalesCosala = 0;
		const numRows = (Object.keys(this.table).length) -1;
		
		this.table.find(rows => {
			rows.find( cell => {
				if (cell.Id_Cell === Id_Cell) {
					this.sheetService.saveValueCell(cell)
						.subscribe(res => console.log(res), error => {
											console.log(error);
											if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
												this.auth.logout();
											}
										});
					price = cell.Price;
					PriceForProduct = parseInt(cell.Value) ? parseInt(cell.Value) * parseFloat(cell.Price) : 0;
					if (cell.CellClass.includes('suma-planta')) {
						pricePlant = true;
					}
				}
				if (cell.Row === row) {
					if (cell.CellClass.includes('suma')) {
						quantity += parseInt(cell.Value) ? parseInt(cell.Value) : 0;
					}
					if (cell.CellClass.includes('total-sum')) {
						allQuantity += quantity;
						cell.Value = quantity ;
						this.sheetService.saveValueCell(cell).subscribe(res => console.log(res), error => {
											console.log(error);
											if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
												this.auth.logout();
											}
										});
					}
					if (cell.CellClass.includes('suma-planta')) {
						allQuantity += parseInt(cell.Value) ? parseInt(cell.Value) : 0;
					}
					if (cell.CellClass.includes('total-quantity')) {
						cell.Value = allQuantity ;
						this.sheetService.saveValueCell(cell).subscribe(res => console.log(res), error => {
											console.log(error);
											if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
												this.auth.logout();
											}
										});
					}
					if (cell.CellClass.includes('sum-price-' + price) && cell.CellClass.includes('suma-cash') && !pricePlant) {
						cell.Value = PriceForProduct;
						this.sheetService.saveValueCell(cell).subscribe(res => console.log(res), error => {
											console.log(error);
											if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
												this.auth.logout();
											}
										});
					}

					if (cell.CellClass.includes('suma-planta')) {
						SumPriceForPlanta = parseInt(cell.Value) ? parseInt(cell.Value) * parseFloat(cell.Price) : 0;
					}
					if (cell.CellClass.includes('total-sale-cash-plant')) {
						cell.Value = SumPriceForPlanta;
						this.sheetService.saveValueCell(cell).subscribe(res => console.log(res), error => {
											console.log(error);
											if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
												this.auth.logout();
											}
										});
					}

					if (cell.CellClass.includes('suma-cash') || cell.CellClass.includes('total-sale-cash-plant')) {
						SumTotalSales += parseFloat(cell.Value) ? parseFloat(cell.Value) : 0;
					}
					if (cell.CellClass.includes('total-sale-cash-all')) {
						cell.Value = SumTotalSales;
						
						this.sheetService.saveValueCell(cell).subscribe(res => console.log(res), error => {
											console.log(error);
											if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
												this.auth.logout();
											}
										});
					}
				}
				if(cell.CellClass.includes('total-sale-cash-all')) {
					this.TotalSalesCosala += parseFloat(cell.Value) > 0 ? parseFloat(cell.Value) : 0;
				}
			});
		});
		//this reset value totales
		this.table[numRows].find( row => {
			if(row.Col > 1 ){
				this.table[numRows][row.Col].Value = null;
			}
		}); 
		//this do sum totales
		this.table.find(rows => { 
			rows.find( cell => {
				if( cell.Col > 1 && cell.Row > 1 && cell.Row < numRows ){
					this.table[numRows][cell.Col].Value += parseInt(cell.Value) ? parseInt(cell.Value) : 0;
				}
			});
		});
		// this save data of totales
		this.sheetService.saveValueCellTotales(this.table[numRows])
			.subscribe(res => console.log(res), error => {
				console.log(error);
				if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
					this.auth.logout();
				}
			});
	}
	/**
	 ###########################################################################################################
	   #######################################    VENTA AJIJIC    ############################################
	 ###########################################################################################################
	*/

	loadSheetAjijic() {
		this.TotalSalesAjijic= 0;
		if (this.periodSelect) {
			this.sheetService.get(this.periodSelect, 'ajijic')
			.subscribe(
				(sheet: any) => {
					this.tableAjijic = sheet;
					
					this.tableAjijic.forEach(row => {
						row.find( cell => {
							if(cell.CellClass.includes('total-sale-cash-all')) {
								this.TotalSalesAjijic += parseFloat(cell.Value) > 0 ? parseFloat(cell.Value) : 0;
							}
						})
					})

				}, (e) => {console.log(e);}
				);	
		}
	}
	/**
	 * this function is to changed quantity in sales and send http request at server
	 * @param Id_Cell
	 * @param val
	 * @param row
	 */
	changedAjijic(Id_Cell: any, row: any) {
		let quantity = 0;
		let allQuantity = 0;
		let PriceForProduct = 0;
		let SumPriceForPlanta = 0;
		let SumTotalSales = 0;
		let price = 0;
		this.TotalSalesAjijic = 0;
		const numRows = (Object.keys(this.tableAjijic).length) -1;
		this.tableAjijic.find(rows => {
			rows.find( cell => {
				if (cell.Id_Cell == Id_Cell) {
					this.sheetService.saveValueCell(cell)
						.subscribe(res => console.log(res), error => {
											console.log(error);
											if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
												this.auth.logout();
											}
										});
					price = cell.Price;
					PriceForProduct = parseInt(cell.Value) ? parseInt(cell.Value) * parseFloat(cell.Price) : 0;
				}
				if (cell.Row == row) {
					if (cell.CellClass.includes('suma')) {
						quantity += parseInt(cell.Value) ? parseInt(cell.Value) : 0;
					}
					if (cell.CellClass.includes('total-sum')) {
						allQuantity += quantity;
						cell.Value = quantity ;
						this.sheetService.saveValueCell(cell).subscribe(res => console.log(res), error => {
											console.log(error);
											if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
												this.auth.logout();
											}
										});
					}
					if (cell.CellClass.includes('suma-planta')) {
						allQuantity += parseInt(cell.Value) ? parseInt(cell.Value) : 0;
					}
					if (cell.CellClass.includes('total-quantity')) {
						cell.Value = allQuantity ;
						this.sheetService.saveValueCell(cell).subscribe(res => console.log(res), error => {
											console.log(error);
											if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
												this.auth.logout();
											}
										});
					}
					if (cell.CellClass.includes('sum-price-' + price)) {
						cell.Value = PriceForProduct;
						this.sheetService.saveValueCell(cell).subscribe(res => console.log(res), error => {
											console.log(error);
											if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
												this.auth.logout();
											}
										});
					}

					if (cell.CellClass.includes('suma-planta')) {
						SumPriceForPlanta = parseInt(cell.Value) ? parseInt(cell.Value) * parseFloat(cell.Price) : 0;
					}
					if (cell.CellClass.includes('total-sale-cash-plant')) {
						cell.Value = SumPriceForPlanta;
						this.sheetService.saveValueCell(cell).subscribe(res => console.log(res), error => {
											console.log(error);
											if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
												this.auth.logout();
											}
										});
					}

					if (cell.CellClass.includes('suma-cash') || cell.CellClass.includes('total-sale-cash-plant')) {
						SumTotalSales += parseFloat(cell.Value) ? parseFloat(cell.Value) : 0;
					}
					if (cell.CellClass.includes('total-sale-cash-all')) {
						this.TotalSalesAjijic = SumTotalSales;
						cell.Value = SumTotalSales;
						this.sheetService.saveValueCell(cell).subscribe(res => console.log(res), error => {
											console.log(error);
											if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
												this.auth.logout();
											}
										});
						
					}
				}
				if(cell.CellClass.includes('total-sale-cash-all')) {
					this.TotalSalesAjijic += parseFloat(cell.Value) > 0 ? parseFloat(cell.Value) : 0;
				}
			});
		});
		//this reset value totales
		this.tableAjijic[numRows].find( row => {
			if(row.Col > 1 ){
				this.tableAjijic[numRows][row.Col].Value = null;
			}
		}); 
		//this do sum totales
		this.tableAjijic.find(rows => { 
			rows.find( cell => {
				if( cell.Col > 1 && cell.Row > 1 && cell.Row < numRows ){
					this.tableAjijic[numRows][cell.Col].Value += parseInt(cell.Value) ? parseInt(cell.Value) : 0;
				}
			});
		});
		// this save data of totales
		this.sheetService.saveValueCellTotales(this.tableAjijic[numRows])
			.subscribe(res => console.log(res), error => {
				console.log(error);
				if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
					this.auth.logout();
				}
			});
	}
	/**
	 ###########################################################################################################
	   ####################################  FIADOS SAN JUAN COSALA  #########################################
	 ###########################################################################################################
	*/
	loadFiados($type) {
		if (this.periodSelect) {
			this.trusting.getTrustPeriod($type, this.selectedPeriod.Id_Period, 'Sin pago')
			.subscribe((res: any) => {
				if ($type === 'ajijic') {
					this.TotalTrustAjijicPeriod= 0;
					this.TotalTrustGarrafoneAjijicPeriod= 0;
					this.tableTrustAjijic = res;
					this.tableTrustAjijic.forEach(row => {
						
						this.TotalTrustAjijicPeriod += parseFloat(row.Total);
						this.TotalTrustGarrafoneAjijicPeriod += Number(row.Quantity);
					})
				} else {
					this.TotalTrustChapalaPeriod= 0;
					this.TotalTrustGarrafoneChapalaPeriod= 0;
					this.tableTrustCosala = res;
					this.tableTrustCosala.forEach(row => {
						this.TotalTrustChapalaPeriod += parseFloat(row.Total);
						this.TotalTrustGarrafoneChapalaPeriod += Number(row.Quantity);
					})
				}
			} , error => {
				console.log(error);
				if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
					this.auth.logout();
				}
			});
		}
	}
	calculaTotalTrustCosala() {
		const Quantity = this.formTrustCosala.value.QuantityCosala;
		const IdProduct = this.formTrustCosala.value.Fk_IdProductCosala;
		this.productsCosala.find( prod =>{
		  if ( prod.Id_Product === IdProduct ) {
			this.$price = parseFloat(prod.Price);
		  }
		});
		this.noteTotalTrustCosala = this.$price * Quantity;
	}
	saveTrustCosala() {
		if (this.formTrustCosala.valid) {
			let data = {Fk_IdPeriod:'',	Price:'',Date:'',Fk_IdClient:'',Fk_IdProduct:'',Note:'',Quantity:'',Fk_IdRoute:''};
			data.Fk_IdPeriod  = this.selectedPeriod.Id_Period;
			data.Price 		  = `${this.$price}`;
			data.Date 		  = this.formTrustCosala.value.DateCosala;
			data.Fk_IdClient  = this.formTrustCosala.value.Fk_IdClientCosala;
			data.Fk_IdProduct = this.formTrustCosala.value.Fk_IdProductCosala;
			data.Note 		  = this.formTrustCosala.value.NoteCosala;
			data.Quantity 	  = this.formTrustCosala.value.QuantityCosala;
			data.Fk_IdRoute   = '1';
			this.trusting.save(data)
				.subscribe((res: any) => {
				if (res.result === 'success') {
					this.formTrustCosala.reset();
					this.loadFiados('cosala');
					this.notifier.notify( 'success', res.alert );
				} else {
					this.notifier.notify( 'error', res.alert );
				}
				}, error => {
				console.log(error);
				if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
					this.auth.logout();
				}
			});
		}
	}
	deleteTrustDayDialog($IdTrust: number, $Client: string, $Quantity: string, $type: string) {
		
			const dialogRef = this.dialog.open(DialogComponent, {
			  minWidth: '350px',
			  data: {title: `Eliminar los fiados de ${$Client}`, content: `¿Desea eliminar los garrafones fiados del cliente ${$Client} de ${$Quantity} garrafones ?`}
			});
			dialogRef.afterClosed().subscribe(result => {
			  if (result === 'confirm') {
				this.trusting.delete($IdTrust)
					.subscribe((res: any) => {
						this.loadFiados($type)
					  	this.notifier.notify( 'info', res.alert );
					}, error => {
					  console.log(error);
					  if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
						this.auth.logout();
					  }
					});
			  }
			});
	}
	calculaTotalTrustAjijic() {
		const Quantity = this.formTrustAjijic.value.QuantityAjijic;
		const IdProduct = this.formTrustAjijic.value.Fk_IdProductAjijic;
		this.productsAjijic.find( prod =>{
		  if ( prod.Id_Product === IdProduct ) {
			this.$price = parseFloat(prod.Price);
		  }
		});
		this.noteTotalTrustAjijic = this.$price * Quantity;
	}
	saveTrustAjijic() {
		if (this.formTrustAjijic.valid) {
			let data = {Fk_IdPeriod:'',	Price:'',Date:'',Fk_IdClient:'',Fk_IdProduct:'',Note:'',Quantity:'',Fk_IdRoute:''};
			data.Fk_IdPeriod  = this.selectedPeriod.Id_Period;
			data.Price 		  = `${this.$price}`;
			data.Date 		  = this.formTrustAjijic.value.DateAjijic;
			data.Fk_IdClient  = this.formTrustAjijic.value.Fk_IdClientAjijic;
			data.Fk_IdProduct = this.formTrustAjijic.value.Fk_IdProductAjijic;
			data.Note 		  = this.formTrustAjijic.value.NoteAjijic;
			data.Quantity 	  = this.formTrustAjijic.value.QuantityAjijic;
			data.Fk_IdRoute   = '2';//ajijic
			this.trusting.save(data)
				.subscribe((res: any) => {
				if (res.result === 'success') {
					this.formTrustAjijic.reset();
					this.loadFiados('ajijic');
					this.notifier.notify( 'success', res.alert );
				} else {
					this.notifier.notify( 'error', res.alert );
				}
				}, error => {
				console.log(error);
				if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
					this.auth.logout();
				}
			});
		}
	}
	/*
	###########################################################################################################
	  ####################################  PAGADOS DEL PERIODO  #########################################
	###########################################################################################################
	*/
	loadNoteForPayment($route: string) {
		let $note =  this.formPaymentAjijic.value.NotePaymentAjijic;
		if( $route === 'cosala'){
			$note =  this.formPaymentCosala.value.NotePaymentCosala;
		}
		if( $note ){
			this.trusting.getNoteForPayment( $note )
			.subscribe((res: any) => {
				this.notePayment = res;
			}, error => {
				console.log(error);
				if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
					this.auth.logout();
				}
			});
		}
	}
	loadPaymentTrusting($type) {
		if (this.periodSelect) {
			this.trusting.getTrustPeriod($type, this.selectedPeriod.Id_Period, 'Pagado')
			.subscribe((res: ClientsDebt[]) => {
				if ($type === 'ajijic') {
					this.TotalPaymentAjijicPeriod 		= 0;
					this.TotalPayGarrafoneAjijicPeriod 	= 0;

					this.tableTrustPaymentAjijic = res;
					this.tableTrustPaymentAjijic.forEach(row => {
						this.TotalPaymentAjijicPeriod 	   += parseFloat(row.Total);
						this.TotalPayGarrafoneAjijicPeriod += Number(row.Quantity);	
					})
				} else {
					this.TotalPaymentChapalaPeriod 		= 0;
					this.TotalPayGarrafoneChapalaPeriod = 0;
					
					this.tableTrustPaymentCosala = res;
					this.tableTrustPaymentCosala.forEach( row => {
						this.TotalPaymentChapalaPeriod 	   += parseFloat(row.Total);
						this.TotalPayGarrafoneChapalaPeriod += Number(row.Quantity);	
					})
				}
			} , error => {
				console.log(error);
				if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
					this.auth.logout();
				}
			});
		}
	}
	savePaymentNotes () {
		if (this.formPaymentCosala.valid) {
			let data = { Fk_IdPeriod:'', Note:'', DatePayment:'' };

			data.Fk_IdPeriod = this.selectedPeriod.Id_Period;
			data.Note 		 = this.formPaymentCosala.value.NotePaymentCosala;
			data.DatePayment = this.formPaymentCosala.value.DatePaymentCosala;
			
			this.trusting.savePayment( data )
				.subscribe((res: any) => {
					if ( res.result === 'success' ) {
						this.formPaymentCosala.reset();
						this.loadPaymentTrusting('cosala');
						this.notifier.notify( 'success', res.alert );
					} else {
						this.notifier.notify( 'error', res.alert );
					}
				}, error => {
					console.log(error);
					if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
						this.auth.logout();
					}
				});
		}
	}
	deletePaymentDialog($IdTrust: number, $Client: string, $Quantity: string, $type: string) {
		
		const dialogRef = this.dialog.open(DialogComponent, {
		  minWidth: '350px',
		  data: {title: `Eliminar el pago de ${$Client}`, content: `¿Desea eliminar el pago del cliente ${$Client} de ${$Quantity} garrafones ?`}
		});
		dialogRef.afterClosed().subscribe(result => {
		  if (result === 'confirm') {
			this.trusting.deletePayment($IdTrust)
				.subscribe((res: any) => {
					this.loadPaymentTrusting($type)
					this.notifier.notify( 'info', res.alert );
				}, error => {
				  console.log(error);
				  if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
					this.auth.logout();
				  }
				});
		  	}
		});
	}
	savePaymentNotesAjijic () {
		if (this.formPaymentAjijic.valid) {
			
			let data = { Fk_IdPeriod:'', Note:'', DatePayment:'' };

			data.Fk_IdPeriod = this.selectedPeriod.Id_Period;
			data.Note 		 = this.formPaymentAjijic.value.NotePaymentAjijic;
			data.DatePayment = this.formPaymentAjijic.value.DatePaymentAjijic;
			
			this.trusting.savePayment( data )
				.subscribe((res: any) => {
					if ( res.result === 'success' ) {
						this.formPaymentAjijic.reset();
						this.loadPaymentTrusting('ajijic');
						this.notifier.notify( 'success', res.alert );
					} else {
						this.notifier.notify( 'error', res.alert );
					}
				}, error => {
					console.log(error);
					if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
						this.auth.logout();
					}
				});
		}
	}
	/*
	###########################################################################################################
	  ####################################  this funcions for tab Expenses  #########################################
	###########################################################################################################
	*/
	deleteExpenseDialog($IdDetail: number, $Expense: string, $Cost) {
		const dialogRef = this.dialog.open(DialogComponent, {
		  minWidth: '350px',
		  data: {title: `Eliminar el gasto ${$Expense}`, content: `¿Desea eliminar el gasto por ${$Expense} de ${$Cost} pesos?`}
		});
		dialogRef.afterClosed().subscribe(result => {
		  if (result === 'confirm') {
			this.expenseSheetService.delete($IdDetail)
				.subscribe((res: any) => {
				  this.loadTableExpenses();				  
				  this.notifier.notify( 'info', res.alert );
				}, error => {
				  console.log(error);
				  if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
					this.auth.logout();
				  }
				});
		  }
		});
	}
	loadTableExpenses() {
		this.TotalExpensesPeriod = 0;
		this.TotalExpensesAjijic = 0;
		this.TotalExpensesCosala = 0;
		const $FkIdPeriod = this.selectedPeriod.Id_Period;
		this.expenseSheetService.get($FkIdPeriod)
			.subscribe((res: any) => {
				this.tableExpenses    = res;
				this.tableExpenses.forEach(row => {
					this.TotalExpensesPeriod += parseFloat(row.Cost);
					if (row.Fk_IdRoute === 2) {
						this.TotalExpensesAjijic += parseFloat(row.Cost);
					} else {
						this.TotalExpensesCosala += parseFloat(row.Cost);
					}
				});
				this.isLoadingResults = false;
			}, error => {
				console.log(error);
				if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
					this.auth.logout();
				}
			});
	}
	saveExpense() {
		this.isLoadingResults = true;
		if (this.formExpenses.valid) {
			let data = this.formExpenses.value;
			data.Fk_IdPeriod = this.selectedPeriod.Id_Period;
			this.expenseSheetService.save(data)
				.subscribe((res: any) => {
				if (res.result === 'success') {
					this.formExpenses.reset();
					this.loadTableExpenses();
					this.notifier.notify( 'success', res.alert );
				} else {
					this.notifier.notify( 'error', res.alert );
				}
				}, error => {
					console.log(error);
					if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
						this.auth.logout();
					}
				});
		}
	}
	/*
	###########################################################################################################
	  ####################################  this funcions for tab Salaris  #########################################
	###########################################################################################################
	*/
	loadTableExployes() {
		/*
		TotalExpensesAjijic
TotalExpensesCosala */
		const $FkIdPeriod 		 = this.selectedPeriod.Id_Period;
		this.totalSalaries 		 = 0;
		this.totalSalariesAjijic = 0;
		this.totalSalariesCosala = 0;
		this.employeService.getSalary($FkIdPeriod)
			.subscribe((res: Employes[]) => {
				this.tableEmployes     = res;
				this.tableEmployes.forEach( ob => {
					this.totalSalaries += parseFloat(ob.Salary);
					if (ob.Fk_IdRoute === 2) {
						this.totalSalariesAjijic += parseFloat(ob.Salary);
					} else {
						this.totalSalariesCosala += parseFloat(ob.Salary);
					}
				})
				this.isLoadingResults = false;
			}, error => {
				console.log(error);
				if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
					this.auth.logout();
				}
			});
	}
	changedSalary(Id_Employe: any, row: any) {
		this.tableEmployes.find( rows => {
			if (rows.Id_Employe == Id_Employe) {
				rows.Fk_IdPeriod = this.selectedPeriod.Id_Period;
				this.employeService.saveSalary(rows)
					.subscribe(res => {
						this.totalSalaries = 0;
						this.tableEmployes.forEach( ob => {
							this.totalSalaries += parseFloat(ob.Salary);
						})
					}, error => {
						if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
							this.auth.logout();
						}
					});
			}
		});
	}
}