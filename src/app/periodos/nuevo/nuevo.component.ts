import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { PeriodosService } from 'src/app/service/periodos.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nuevo',
  templateUrl: './nuevo.component.html',
  styleUrls: ['./nuevo.component.css']
})
export class NuevoComponent implements OnInit {
  form: FormGroup;
  submitted: boolean =  false;
  constructor(private formBuilder: FormBuilder, private period:PeriodosService, private router: Router) {
    this.form =  this.formBuilder.group({
      DateInin: ['', Validators.required],
      DateEnd: ['', [ Validators.required ]]
    });
  }
  ngOnInit() {
  }
  savePeriod() {
    if (this.form.valid) {
      this.period.save(this.form.value).subscribe(
        (dat) => {
          this.router.navigate(['/']);
        }, (e) => {
          console.log(e);
        });
    }
  }

}
