import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ClientsService } from 'src/app/service/clients.service';
import { MatSnackBar } from '@angular/material';
import { RoutesService } from 'src/app/service/routes.service';
import { RoutesSales } from 'src/app/interfaces/routes';
import { Clients } from '../clients';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  form: FormGroup;
  submitted: boolean =  false;
  Routes: RoutesSales;
  DataClient: Clients;
  IdClient: any;
  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private clientService: ClientsService,
              private snack: MatSnackBar,
              private routeService: RoutesService,
              private ActiveRoute: ActivatedRoute) {
                this.IdClient = this.ActiveRoute.snapshot.paramMap.get('IdClient');
                this.form =  this.formBuilder.group({
                  Client: ['', Validators.required],
                  PhoneNumber: ['',[Validators.required]],
                  Address: ['',[Validators.required]],
                  Colony: ['',[Validators.required]],
                  Fk_IdRoute: ['',[Validators.required]],
                });
                this.routeService.get()
                  .subscribe((res: any) => {
                    this.Routes = res;
                }, (e) => console.log(e))
                this.clientService.getById(this.IdClient)
                  .subscribe((res: any) => {
                    this.DataClient = res;
                  }, (e) => console.log(e) );
  }

  ngOnInit() {
  }
  editClient($IdClient: number) {
    if ( this.form.valid) {
      this.clientService.edit($IdClient, this.form.value)
          .subscribe((res: any) => {
            if (res.result === 'success') {
              this.router.navigate(['/catalogos/clientes/']);
            }
            this.snack.open(res.alert, 'Cerrar', { duration: 9000, verticalPosition: 'top' });
          }, (e) => console.log(e));
    }
  }
}
