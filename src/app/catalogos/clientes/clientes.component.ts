import { Component, OnInit, ViewChild } from '@angular/core';
import { ClientsService } from 'src/app/service/clients.service';
import { Clients } from './clients';
import { MatDialog, MatSnackBar, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { SearchClients } from './search-clients';
import { DialogComponent } from '../../partials/dialog/dialog.component';
import { AuthService } from 'src/app/service/auth.service';
@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css']
})

export class ClientesComponent implements OnInit {
  dataSource:MatTableDataSource<Clients>;
  displayedColumns: string[] = ['Options', 'Client', 'PhoneNumber', 'Address', 'Fk_IdRoute'];
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  isLoadingResults = false;
  resultsLength: any = 0;
  pageIndex: any = 0;
  search: SearchClients = { client: '', phone: '', address: '', route: '' };
  constructor(
    private clientService: ClientsService,
    public dialog: MatDialog,
    private auth: AuthService,
    public snack: MatSnackBar) {
      this.loadClients();
  }
  ngOnInit() {
  }
  loadClients($order = {active: 'Client', direction: 'asc'}, $paginator = {pageIndex: 0, pageSize: 25}) {
    this.isLoadingResults = true;

    if (!Object.entries($order).length) {
      $order = { active: 'Client', direction: 'asc' };
    }
    if (!Object.entries($paginator).length) {
      $paginator = {pageIndex: 0, pageSize: 25};
    } else {
      this.pageIndex = $paginator.pageIndex;
    }
    this.clientService.get( $order, $paginator, this.search)
        .subscribe((res: any) => {
          this.isLoadingResults     = false;
          this.dataSource           =  res.results;
          this.dataSource.sort      = this.sort;
          this.dataSource.paginator = this.paginator;
          this.resultsLength        = res.SumRecords;

          this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
        }, error => {
          console.log(error);
          if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
            this.auth.logout();
          }
        });
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  deleteClientDialog($IdClient: number, $client: string) {
    const dialogRef = this.dialog.open(DialogComponent, {
      minWidth: '350px',
      data: {title: `Eliminar a ${$client}`, content: `¿Desea eliminar al clientes ${$client} ?`}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === 'confirm') {
        this.clientService.delete( $IdClient )
            .subscribe((res: any) => {
              this.loadClients();
              this.snack.open(res.alert, 'Cerrar', { duration: 9000, verticalPosition: 'top' });
            }, (e) => console.log(e));
      }
    });
  }
}

