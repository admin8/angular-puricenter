export interface Clients {
    Options?: string;
    Id_Client: number;
    Client: string;
    PhoneNumber?: string;
    Address?: string;
    Colony?: string;
    Fk_IdCity?: number;
    Fk_IdRoute: number;
    token?: string;
}
