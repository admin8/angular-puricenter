import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ClientsService } from 'src/app/service/clients.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { RoutesService } from 'src/app/service/routes.service';
import { RoutesSales } from 'src/app/interfaces/routes';
import { AuthService } from 'src/app/service/auth.service';
@Component({
  selector: 'app-nuevo',
  templateUrl: './nuevo.component.html',
  styleUrls: ['./nuevo.component.css']
})
export class NuevoComponent implements OnInit {
  form: FormGroup;
  submitted: boolean =  false;
  Routes: RoutesSales;
  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private clientService: ClientsService,
              private snack: MatSnackBar,
              private routeService: RoutesService,
              private auth: AuthService) {
                this.form =  this.formBuilder.group({
                  Client: ['', Validators.required],
                  PhoneNumber: ['', [Validators.required]],
                  Address: ['', [Validators.required]],
                  Colony: ['', [Validators.required]],
                  Fk_IdRoute: ['', [Validators.required]],
                });
                this.routeService.get()
                  .subscribe((res:any) => {
                    this.Routes = res;
                  }, error => {
                    console.log(error);
                    if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
                      this.auth.logout();
                    }
                  });
              }

  ngOnInit() {
  }
  saveClient(){
    if(this.form.valid){
      this.clientService.save(this.form.value)
          .subscribe((res:any) => {
            if (res.result==='success') {
              this.router.navigate(['/catalogos/clientes/']);
            }
            this.snack.open(res.alert, 'Cerrar', { duration: 9000, verticalPosition: 'top' });
          }, error => {
            console.log(error);
            if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
              this.auth.logout();
            }
          });
    }
  }
}
