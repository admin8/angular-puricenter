export interface SearchClients {
    client?: string;
    phone?: string;
    address?: string;
    route?: string;
}
