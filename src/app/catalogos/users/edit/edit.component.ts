import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { NotifierService } from 'angular-notifier';
import { AuthService } from 'src/app/service/auth.service';
import { RoutesSales } from 'src/app/interfaces/routes';
import { RoutesService } from 'src/app/service/routes.service';
import { User } from 'src/app/interfaces/user';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  form: FormGroup;
  submitted = false;
  Routes: RoutesSales;
  IdUser: any;
  formData: User = { id: null , name: '', user: '', route: '',  Active: '', type: '', email: '', UserType: '', Fk_IdRoute: null };
  private notifier: NotifierService;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private usersSerivce: UserService,
    private route: ActivatedRoute,
    private auth: AuthService,
    notifier: NotifierService,
    private routeService: RoutesService
  ) {
    this.notifier = notifier;
    this.form =  this.formBuilder.group({
      name: ['', Validators.required],
      user: ['', Validators.required],
      email: [''],
      Fk_IdRoute: ['', Validators.required],
      UserType: ['', Validators.required],
      Active: ['', Validators.required],
      password: ['']
    });
    this.routeService.get()
        .subscribe((res: any) => {
          this.Routes = res;
        }, error => {
          console.log(error);
          if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
            this.auth.logout();
          }
        });
    this.IdUser = this.route.snapshot.paramMap.get('IdUser');
    if (this.IdUser) {
      this.usersSerivce.getOne(this.IdUser)
        .subscribe( (res: User) => {
          this.formData = res;
          this.formData.UserType = this.formData.UserType === 'Administrador' ? '1' : this.formData.UserType === 'Repartidor' ? '2' : '3';
        }, error => {
          console.log(error);
          if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
            this.auth.logout();
          }
        });
    }
  }
  ngOnInit() {
  }
  save() {
    if (this.form.valid) {
      this.usersSerivce.edit(this.form.value, this.IdUser)
          .subscribe((res: any) => {
            if (res.result === 'success') {
              this.router.navigate(['/catalogos/users']);
              this.notifier.notify( 'success', res.alert );
            } else {
              this.notifier.notify( 'error', res.alert );
            }
          }, error => {
            console.log(error);
            if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
              this.auth.logout();
            }
          });
    }
  }
}
