import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { AuthService } from 'src/app/service/auth.service';
import { NotifierService } from 'angular-notifier';
import { DialogComponent } from 'src/app/partials/dialog/dialog.component';
import { UserService } from 'src/app/service/user.service';
import { User } from 'src/app/interfaces/user';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  private notifier: NotifierService;
  dataSource: MatTableDataSource<User>;
  displayedColumns: string[] = ['Options', 'name', 'user', 'email', 'Route', 'UserType', 'Active'];
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  isLoadingResults = false;
  resultsLength: any = 0;
  pageIndex: any = 0;
  search: User = { name: '', user: '', email: '', route: '', UserType: '', Active: '' };
  constructor(
    private UsersService: UserService,
    public dialog: MatDialog,
    private auth: AuthService,
    notifier: NotifierService) {
      this.notifier = notifier;
      this.loadData();
  }
  ngOnInit() {
  }
  loadData($order = {active: 'name', direction: 'asc'}, $paginator = {pageIndex: 0, pageSize: 25}) {
    this.isLoadingResults = true;

    if (!Object.entries($order).length) {
      $order = { active: 'name', direction: 'asc' };
    }
    if (!Object.entries($paginator).length) {
      $paginator = {pageIndex: 0, pageSize: 25};
    } else {
      this.pageIndex = $paginator.pageIndex;
    }
    this.UsersService.get( $order, $paginator, this.search )
        .subscribe(( res: any ) => {
          this.isLoadingResults     = false;
          this.dataSource           =  res.results;
          this.dataSource.sort      = this.sort;
          this.dataSource.paginator = this.paginator;
          this.resultsLength        = res.SumRecords;

          this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
        }, error => {
          console.log(error);
          if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
            this.auth.logout();
          }
        });
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  deleteDialog($id: number, $string: string) {
    const dialogRef = this.dialog.open(DialogComponent, {
      minWidth: '350px',
      data: {title: `Eliminar a usuario`, content: `¿Desea eliminar el usuario ${$string}?`}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === 'confirm') {
        this.UsersService.delete( $id )
            .subscribe((res: any) => {
              this.loadData();
              this.notifier.notify( 'success', res.alert );
            }, error => {
              console.log(error);
              if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
                this.auth.logout();
              }
            });
      }
    });
  }
}
