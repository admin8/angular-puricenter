import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { NotifierService } from 'angular-notifier';
import { AuthService } from 'src/app/service/auth.service';
import { ExpensesService } from 'src/app/service/expenses';

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.css']
})
export class NewComponent implements OnInit {
  form: FormGroup;
  submitted = false;
  private notifier: NotifierService;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private auth: AuthService,
    notifier: NotifierService,
    private expenseService: ExpensesService
  ) {
    this.notifier = notifier;
    this.form =  this.formBuilder.group({
      Expense: ['', Validators.required]
    });
  }
  ngOnInit() {
  }
  save() {
    if (this.form.valid) {
      this.expenseService.save(this.form.value)
          .subscribe((res: any) => {
            if (res.result === 'success') {
              this.router.navigate(['/catalogos/expenses']);
              this.notifier.notify( 'success', res.alert );
            } else {
              this.notifier.notify( 'error', res.alert );
            }
          }, error => {
            console.log(error);
            if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
              this.auth.logout();
            }
          });
    }
  }

}
