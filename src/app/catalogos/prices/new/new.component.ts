import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { NotifierService } from 'angular-notifier';
import { AuthService } from 'src/app/service/auth.service';
import { Products } from 'src/app/interfaces/products';
import { ProductsService } from 'src/app/service/products.service';
import { RoutesSales } from 'src/app/interfaces/routes';
import { RoutesService } from 'src/app/service/routes.service';
@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.css']
})
export class NewComponent implements OnInit {
  form: FormGroup;
  submitted = false;
  Routes: RoutesSales;
  private notifier: NotifierService;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private productService: ProductsService,
    private route: ActivatedRoute,
    private auth: AuthService,
    notifier: NotifierService,
    private routeService: RoutesService
  ) {
    this.notifier = notifier;
    this.form =  this.formBuilder.group({
      Price: ['', Validators.required],
      Fk_IdRoute: ['', Validators.required]
    });
    this.routeService.get()
        .subscribe((res: any) => {
          this.Routes = res;
      }, (e) => console.log(e) );
  }
  ngOnInit() {
  }
  save() {
    if (this.form.valid) {
      this.productService.save(this.form.value)
          .subscribe((res: any) => {
            if (res.result === 'success') {
              this.router.navigate(['/catalogos/prices']);
              this.notifier.notify( 'success', res.alert );
            } else {
              this.notifier.notify( 'error', res.alert );
            }
          }, error => {
            console.log(error);
            if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
              this.auth.logout();
            }
          });
    }
  }

}
