import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog, MatSnackBar } from '@angular/material';
import { Prices } from 'src/app/interfaces/Prices';
import { AuthService } from 'src/app/service/auth.service';
import { NotifierService } from 'angular-notifier';
import { DialogComponent } from 'src/app/partials/dialog/dialog.component';
import { ProductsService } from 'src/app/service/products.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  private notifier: NotifierService;
  dataSource: MatTableDataSource<Prices>;
  displayedColumns: string[] = ['Options', 'Price', 'Route', 'Description'];
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  isLoadingResults = false;
  resultsLength: any = 0;
  pageIndex: any = 0;
  search: Prices = { Price: '', Route: '', Description: '' };
  constructor(
    private ProductServices: ProductsService,
    public dialog: MatDialog,
    private auth: AuthService,
    public snack: MatSnackBar,
    notifier: NotifierService) {
      this.notifier = notifier;
      this.loadData();
  }
  ngOnInit() {
  }
  loadData($order = {active: 'Price', direction: 'asc'}, $paginator = {pageIndex: 0, pageSize: 25}) {
    this.isLoadingResults = true;

    if (!Object.entries($order).length) {
      $order = { active: 'Price', direction: 'asc' };
    }
    if (!Object.entries($paginator).length) {
      $paginator = {pageIndex: 0, pageSize: 25};
    } else {
      this.pageIndex = $paginator.pageIndex;
    }
    this.ProductServices.getAll( $order, $paginator, this.search )
        .subscribe(( res: any ) => {
          this.isLoadingResults     = false;
          this.dataSource           =  res.results;
          this.dataSource.sort      = this.sort;
          this.dataSource.paginator = this.paginator;
          this.resultsLength        = res.SumRecords;

          this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
        }, error => {
          console.log(error);
          if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
            this.auth.logout();
          }
        });
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  deleteDialog($IdPrice: number, $Price: string) {
    const dialogRef = this.dialog.open(DialogComponent, {
      minWidth: '350px',
      data: {title: `Eliminar a precio`, content: `¿Desea eliminar el precio ${$Price} pesos?`}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === 'confirm') {
        this.ProductServices.delete( $IdPrice )
            .subscribe((res: any) => {
              this.loadData();
              this.notifier.notify( 'success', res.alert );
            }, error => {
              console.log(error);
              if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
                this.auth.logout();
              }
            });
      }
    });
  }

}
