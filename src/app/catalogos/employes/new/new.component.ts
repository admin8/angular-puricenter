import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { EmployesService } from 'src/app/service/employes.service';
import { NotifierService } from 'angular-notifier';
import { Employes } from 'src/app/interfaces/employes';
import { AuthService } from 'src/app/service/auth.service';
import { RoutesSales } from 'src/app/interfaces/routes';
import { RoutesService } from 'src/app/service/routes.service';
@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.css']
})
export class NewComponent implements OnInit {
  form: FormGroup;
  submitted: boolean = false;
  IdEmploye: any;
  Routes: RoutesSales;
  dataEmploye: Employes = {Id_Employe: 0, Name: '', Position: '', Salary: null, Fk_IdRoute: null,  Active: null};
  private notifier: NotifierService;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private employeService: EmployesService,
    private route: ActivatedRoute,
    private auth: AuthService,
    private routeService: RoutesService,
    notifier: NotifierService
  ) {
    this.notifier = notifier;
    this.form =  this.formBuilder.group({
      Name: ['', Validators.required],
      Position: ['', [Validators.required]],
      Salary: ['', [Validators.required]],
      Fk_IdRoute: ['', [Validators.required]],
      Active: ['', [Validators.required]],
    });
    this.IdEmploye = this.route.snapshot.paramMap.get('IdEmploye');
    if (this.IdEmploye) {
      this.employeService.getOne(this.IdEmploye)
        .subscribe( (res: Employes) => {
          this.dataEmploye = res;
        }, error => {
          console.log(error);
          if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
            this.auth.logout();
          }
        });
    }
    this.routeService.get()
      .subscribe((res: any) => {
        this.Routes = res;
    }, (e) => console.log(e) );
  }
  ngOnInit() {
  }
  saveEmploye() {
    if (this.form.valid) {
      if (this.IdEmploye) {
        this.employeService.edit(this.form.value, this.dataEmploye.Id_Employe )
        .subscribe((res: any) => {
          if (res.result === 'success') {
            this.router.navigate(['/catalogos/employes/']);
            this.notifier.notify( 'success', res.alert );
          }
        }, error => {
          console.log(error);
          if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
            this.auth.logout();
          }
        });
      } else {
        this.employeService.save(this.form.value)
        .subscribe((res: any) => {
          if (res.result === 'success') {
            this.router.navigate(['/catalogos/employes/']);
            this.notifier.notify( 'success', res.alert );
          }
        }, error => {
          console.log(error);
          if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
            this.auth.logout();
          }
        });
      }
    }
  }

}
