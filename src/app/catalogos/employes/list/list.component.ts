import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog, MatSnackBar } from '@angular/material';
import { Employes } from 'src/app/interfaces/employes';
import { EmployesService } from 'src/app/service/employes.service';
import { AuthService } from 'src/app/service/auth.service';
import { SearchEmployes } from 'src/app/interfaces/search-employes';
import { NotifierService } from 'angular-notifier';
import { DialogComponent } from 'src/app/partials/dialog/dialog.component';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  private notifier: NotifierService;
  dataSource:MatTableDataSource<Employes>;
  displayedColumns: string[] = ['Options', 'Name', 'Position', 'Salary', 'Route', 'Active'];
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  isLoadingResults = false;
  resultsLength: any = 0;
  pageIndex: any = 0;
  search: SearchEmployes = { Name: '', Position: '', Salary: null, Route: null, Active: null };
  constructor(
    private employeService: EmployesService,
    public dialog: MatDialog,
    private auth: AuthService,
    public snack: MatSnackBar,
    notifier: NotifierService) {
      this.notifier = notifier;
      this.loadEmployes();
  }
  ngOnInit() {
  }
  loadEmployes($order = {active: 'Name', direction: 'asc'}, $paginator = {pageIndex: 0, pageSize: 25}) {
    this.isLoadingResults = true;

    if (!Object.entries($order).length) {
      $order = { active: 'Name', direction: 'asc' };
    }
    if (!Object.entries($paginator).length) {
      $paginator = {pageIndex: 0, pageSize: 25};
    } else {
      this.pageIndex = $paginator.pageIndex;
    }
    this.employeService.get( $order, $paginator, this.search )
        .subscribe((res: any) => {
          this.isLoadingResults     = false;
          this.dataSource           =  res.results;
          this.dataSource.sort      = this.sort;
          this.dataSource.paginator = this.paginator;
          this.resultsLength        = res.SumRecords;

          this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
        }, error => {
          console.log(error);
          if ( error.error.message === 'Unauthenticated.' || error.statusText === 'Unauthorized' ) {
            this.auth.logout();
          }
        });
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  deleteEmployeDialog($IdEmploye: number, $Employe: string) {
    const dialogRef = this.dialog.open(DialogComponent, {
      minWidth: '350px',
      data: {title: `Eliminar a ${$Employe}`, content: `¿Desea eliminar al empleado ${$Employe} ?`}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === 'confirm') {
        this.employeService.delete( $IdEmploye )
            .subscribe((res: any) => {
              this.loadEmployes();
              this.notifier.notify( 'success', res.alert );
            }, (e) => console.log(e));
      }
    });
  }
}
