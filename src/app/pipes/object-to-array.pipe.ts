import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'objectToArray'
})
export class ObjectToArrayPipe implements PipeTransform {
  /**
   * transform object in array for reading data with ngfor
   * @param $Table object
   * @param args optionals
   */
  transform($Table: any, args?: any): any {
    let table = [];
		for(let key in $Table){
			if($Table.hasOwnProperty(key)){
				table.push($Table[key]);
			}
		}
		return table;
  }
}
