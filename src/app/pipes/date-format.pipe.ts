import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';
import { Constants } from '../ultilities/Constants';

@Pipe({
  name: 'dateFormat'
})
export class DateFormatPipe extends DatePipe implements PipeTransform {
  /**
   * transform date 2018-01-12 to 12/01/2018 
   * @param date string with date 2018-01-12
   * @param args 
   */
  transform(date: any, args?:any):any {
    return super.transform(date, Constants.DATE_FMT);
  }

}
