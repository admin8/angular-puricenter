import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import 'hammerjs';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MaterialModule } from './material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NotifierModule, NotifierOptions } from 'angular-notifier';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { HomeComponent } from './home/home.component';
import { ObjectToArrayPipe } from './pipes/object-to-array.pipe';
import { DateFormatPipe } from './pipes/date-format.pipe';
import { NuevoComponent } from './periodos/nuevo/nuevo.component';
import { DialogComponent } from './partials/dialog/dialog.component';
import { CortesComponent } from './rutas/cortes/cortes.component';
import { ClientesComponent } from './catalogos/clientes/clientes.component';
import { NuevoComponent as NewClientComponent } from './catalogos/clientes/nuevo/nuevo.component';
import { EditComponent as EditClientComponent } from './catalogos/clientes/edit/edit.component';
import { LoginComponent } from './pages/auth/login/login.component';
import { ListDebtComponent } from './pages/debt/list-debt/list-debt.component';
import { DetailDebtComponent } from './pages/debt/detail-debt/detail-debt.component';
import { ListComponent as ListEmployesComponent } from './catalogos/employes/list/list.component';
import { NewComponent as NewEmployeComponent } from './catalogos/employes/new/new.component';
import { ListComponent as ListDebtSheetComponent } from './pages/debt-sheet/list/list.component';
import { DetailComponent as DetailDebtSheetComponent } from './pages/debt-sheet/detail/detail.component';
import { ListComponent as ListPricesComponent } from './catalogos/prices/list/list.component';
import { NewComponent as NewPriceComponent } from './catalogos/prices/new/new.component';
import { ListComponent as ListUserComponent } from './catalogos/users/list/list.component';
import { NewComponent as NewUserComponent } from './catalogos/users/new/new.component';
import { EditComponent as EditUserComponent } from './catalogos/users/edit/edit.component';
import { ListComponent as ListExpensesComponent} from './catalogos/expenses/list/list.component';
import { NewComponent as NewExpensesComponent} from './catalogos/expenses/new/new.component';
import { ListComponent as ListSalesClientComponent } from './pages/client-sales/list/list.component';
import { DetailComponent as DetailSalesClientComponent } from './pages/client-sales/detail/detail.component';
import { BillDialogComponent } from './pages/client-sales/bill-dialog/bill-dialog.component';

const customNotifierOptions: NotifierOptions = {
  position: {
    horizontal: {
      position: 'left',
      distance: 12
    },
    vertical: {
      position: 'bottom',
      distance: 12,
      gap: 10
    }
  },
  theme: 'material',
  behaviour: {
    autoHide: 10000,
    onClick: 'hide',
    onMouseover: 'pauseAutoHide',
    showDismissButton: true,
    stacking: 4
  },
  animations: {
    enabled: true,
    show: {
      preset: 'slide',
      speed: 300,
      easing: 'ease'
    },
    hide: {
      preset: 'fade',
      speed: 300,
      easing: 'ease',
      offset: 50
    },
    shift: {
      speed: 300,
      easing: 'ease'
    },
    overlap: 150
  }
};

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NuevoComponent,
    ObjectToArrayPipe,
    DateFormatPipe,
    DialogComponent,
    CortesComponent,
    ClientesComponent,
    NewClientComponent,
    EditClientComponent,
    LoginComponent,
    ListDebtComponent,
    DetailDebtComponent,
    ListEmployesComponent,
    NewEmployeComponent,
    ListDebtSheetComponent,
    DetailDebtSheetComponent,
    ListPricesComponent,
    NewPriceComponent,
    ListUserComponent,
    NewUserComponent,
    EditUserComponent,
    ListExpensesComponent,
    NewExpensesComponent,
    ListSalesClientComponent,
    DetailSalesClientComponent,
    BillDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
    FormsModule, ReactiveFormsModule, NotifierModule.withConfig(customNotifierOptions)
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [DialogComponent, BillDialogComponent]
})
export class AppModule { }
