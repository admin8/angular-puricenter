import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { NuevoComponent } from './periodos/nuevo/nuevo.component';
import { CortesComponent } from './rutas/cortes/cortes.component';
import { ClientesComponent } from './catalogos/clientes/clientes.component';
import { NuevoComponent as NewClientComponent } from './catalogos/clientes/nuevo/nuevo.component';
import { EditComponent as EditClientComponent } from './catalogos/clientes/edit/edit.component';
import { AuthGuard } from './service/auth.guard';
import { LoginComponent } from './pages/auth/login/login.component';
import { ListDebtComponent } from './pages/debt/list-debt/list-debt.component';
import { DetailDebtComponent } from './pages/debt/detail-debt/detail-debt.component';
import { ListComponent as ListEmployesComponent } from './catalogos/employes/list/list.component';
import { NewComponent as NewEmployeComponent } from './catalogos/employes/new/new.component';
import { ListComponent as ListDebtSheetComponent } from './pages/debt-sheet/list/list.component';
import { DetailComponent as DetailDebtSheetComponent } from './pages/debt-sheet/detail/detail.component';
import { ListComponent as ListPricesComponent } from './catalogos/prices/list/list.component';
import { NewComponent as NewPriceComponent } from './catalogos/prices/new/new.component';
import { ListComponent as ListUserComponent } from './catalogos/users/list/list.component';
import { NewComponent as NewUserComponent } from './catalogos/users/new/new.component';
import { EditComponent as EditUserComponent } from './catalogos/users/edit/edit.component';
import { RoleGuardService } from './service/role-guard.service';
import { ListComponent as ListExpensesComponent} from './catalogos/expenses/list/list.component';
import { NewComponent as NewExpensesComponent} from './catalogos/expenses/new/new.component';
import { ListComponent as ListSalesClientComponent } from './pages/client-sales/list/list.component';
import { DetailComponent as DetailSalesClientComponent } from './pages/client-sales/detail/detail.component';
const routes: Routes = [

  {path: '', component: HomeComponent, pathMatch: 'full', canActivate: [RoleGuardService], data: { role: 'Administrador' } },
  {path: 'login', component: LoginComponent},
  {path: 'nuevo', component: NuevoComponent, canActivate: [RoleGuardService] , data: { role: 'Administrador' }  },
  {path: 'debt/list', component: ListDebtComponent, canActivate: [RoleGuardService] , data: { role: 'Repartidor' }  },
  {path: 'debt/detail/:IdClient', component: DetailDebtComponent, canActivate: [RoleGuardService] , data: { role: 'Repartidor' }  },
  {path: 'rutas/cortes', component: CortesComponent, canActivate: [RoleGuardService] , data: { role: 'Repartidor' }  },
  {path: 'catalogos/employes', component: ListEmployesComponent, canActivate: [RoleGuardService] , data: { role: 'Administrador' }  },
  {path: 'debt-sheet/list', component: ListDebtSheetComponent, canActivate: [RoleGuardService] , data: { role: 'Administrador' }  },
  {path: 'debt-sheet/detail/:IdClient', component: DetailDebtSheetComponent, canActivate: [RoleGuardService] , data: { role: 'Administrador' }},
  
  {path: 'sales/list', component: ListSalesClientComponent, canActivate: [RoleGuardService] , data: { role: 'Administrador' }  },
  {path: 'sales/detail/:IdClient', component: DetailSalesClientComponent, canActivate: [RoleGuardService] , data: { role: 'Administrador' }},


  {path: 'catalogos/employes/new', component: NewEmployeComponent, canActivate: [RoleGuardService] , data: { role: 'Administrador' }  },
  {path: 'catalogos/employes/new/:IdEmploye', component: NewEmployeComponent, canActivate: [RoleGuardService] , data: { role: 'Administrador' }},
  {path: 'catalogos/clientes', component: ClientesComponent, canActivate: [AuthGuard]},
  {path: 'catalogos/clientes/nuevo', component: NewClientComponent, canActivate: [AuthGuard]},
  {path: 'catalogos/clientes/edit/:IdClient', component: EditClientComponent, canActivate: [AuthGuard]},
  {path: 'catalogos/prices', component: ListPricesComponent, canActivate: [RoleGuardService] , data: { role: 'Administrador' }  },
  {path: 'catalogos/prices/new', component: NewPriceComponent, canActivate: [RoleGuardService] , data: { role: 'Administrador' }  },
  
  {path: 'catalogos/expenses', component: ListExpensesComponent, canActivate: [RoleGuardService] , data: { role: 'Administrador' }  },
  {path: 'catalogos/expenses/new', component: NewExpensesComponent, canActivate: [RoleGuardService] , data: { role: 'Administrador' }  },
  
  {path: 'catalogos/users', component: ListUserComponent, canActivate: [RoleGuardService] , data: { role: 'Super usuario' }  },
  {path: 'catalogos/users/new', component: NewUserComponent, canActivate: [RoleGuardService] , data: { role: 'Super usuario' } },
  {path: 'catalogos/users/edit/:IdUser', component: EditUserComponent, canActivate: [RoleGuardService] , data: { role: 'Super usuario' }  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
