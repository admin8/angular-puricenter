import { NgModule } from "@angular/core";
import {MatIconModule,
        MatTabsModule,
        MatTableModule,
        MatSelectModule,
        MatDividerModule,
        MatToolbarModule,
        MatCardModule,
        MatGridListModule,
        MatButtonModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatFormFieldModule,
        MatInputModule,
        MatDialogModule,
        MatSnackBarModule,
        MatMenuModule,
        MatPaginatorModule,
        MatSortModule,
        MatProgressSpinnerModule,
        MatListModule,
        MatTooltipModule,
        MatCheckboxModule
    } from '@angular/material';
const modules = [
    MatIconModule,
    MatTabsModule,
    MatTableModule,
    MatSelectModule,
    MatDividerModule,
    MatToolbarModule,
    MatCardModule,
    MatGridListModule,
    MatButtonModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatDialogModule,
    MatSnackBarModule,
    MatMenuModule,
    MatPaginatorModule,
    MatSortModule,
    MatProgressSpinnerModule,
    MatListModule,
    MatTooltipModule,
    MatCheckboxModule
]

@NgModule({
    imports:modules,
    exports: modules
})
export class MaterialModule{}